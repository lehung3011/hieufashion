<?php 
namespace App\Helpers;
use Config;
use Illuminate\Support\Facades\URL;

class Form {
    public static function show ($elements , $params = null) { 
        $xhtml = null;
        foreach ($elements as $element) {
            $xhtml .= self::formGroup($element , $params);
        }
        return $xhtml;
    }

    public static function formGroup ($element, $params = null) {
        $type = isset($element['type']) ? $element['type'] : "input";
        $xhtml = null;

        $class = (isset($params['class'])) ? $params['class'] : 'col-md-6 col-sm-6 col-xs-12';
        $note = (isset($element['note'])) ? wrap_text($element['note'], 'description', 'p') : null;

        switch ($type) {
            case 'input':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                            %s
                        </div>
                    </div>', $element['label'], $element['element'], $note
                );
                break;
            case 'thumb':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                            <div id="holder">%s</div>
                        </div>
                    </div>', $element['label'], $element['element'], $element['thumb']
                );
                break;
            case 'avatar':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                            <div id="holder">%s</div>
                        </div>
                    </div>', $element['label'], $element['element'], $element['avatar']
                );
                break;
            case 'image':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                            <div id="holder">%s</div>
                        </div>
                    </div>', $element['label'], $element['element'], $element['image']
                );
                break;
            case 'image-button':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        %s
                        <div class="'.$class.'">
                            %s
                        </div>
                    </div>', $element['label'], $element['element']
                );
                break;
            case 'btn-submit':
                $xhtml .= sprintf(
                    '<div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            %s
                        </div>
                    </div>', $element['element']
                );
                break;
            case 'btn-submit-edit':
                $xhtml .= sprintf(
                    '<div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                            %s
                        </div>
                    </div>', $element['element']
                );
                break;
            case 'btn-button':
                $xhtml .= sprintf(
                    '<div class="form-group">
                        <div class="col-xs-12">
                            %s
                        </div>
                    </div>', $element['element']
                );
                break;
        }

        return $xhtml;
    }
    
    public static function input_image($name, $value, $option = null)
    {
        $xhtml = null;
        $xhtml .= sprintf(
                    '<div class="input-group">
                        <span class="input-group-btn">
                            <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary text-white"><i class="fa fa-picture-o"></i> Choose</a>
                        </span>
                        <input id="thumbnail" class="form-control" type="text" name="%s" value="%s">
                        
                    </div>', $name, $value
                );
        return $xhtml;
    }
    
    public static function button_image($name, $value, $option = null)
    {
        $xhtml = null;
        $xhtmlThumb = null;
        $hideButtonUpload = null;
        if(isset($option['thumb']) && !empty($option['thumb']))
        {
            $xhtmlThumb .= $option['thumb'];
            $xhtmlThumb .= '<button id="btn-delete-lfm" class="btn btn-danger" style="margin-top: 10px;" type="button"><i class="fa fa-trash-o"></i> Xóa ảnh</button>';
            $hideButtonUpload = 'hide';
        }
        $xhtml .= sprintf(
                    '<div class="button-group">
                        <button type="button" id="btn-upload-lfm" data-input="thumbnail" data-preview="holder-image" class="btn btn-primary text-white '.$hideButtonUpload.'"><i class="fa fa-picture-o"></i> Chọn ảnh</button>
                        <input id="thumbnail" class="form-control" type="hidden" name="%s" value="%s">
                        <div id="holder-image">%s</div>
                    </div>', $name, $value, $xhtmlThumb
                );
        return $xhtml;
    }

    public static function post_title($value, $name = 'title', $option = null)
    {
        $xhtml = null;
        $xhtml .= sprintf('<div class="post-title">
                                <input type="text" id="title" class="form-control col-xs-12" placeholder="Nhập tiêu đề tại đây" name="%s" value="%s" autofocus=1 required>
                            </div>', $name, $value
                );
        return $xhtml;
    }
    public static function post_slug($slug, $name='friendly_url', $option = null)
    {
        $url = URL::to('/'.$slug);
		$cls = (empty($slug)) ? 'style="z-index:-1;"' : '';
		$xhtml = null;
		$xhtml .= '<div id="edit-slug-box" '.$cls.'>';
		$xhtml .= '<strong>Liên kết tĩnh:</strong>';
		$xhtml .= '<span id="sample-permalink">';
		$xhtml .= sprintf('<a href="%s" target="_blank">%s<span id="editable-post-name">%s</span></a>', $url, url('/').'/', $slug);
		$xhtml .= sprintf('<input type="hidden" name="%s" value="%s" class="post-slug form-control" autocomplete="off"/>', $name, $slug);
		$xhtml .= '</span>';
		$xhtml .= '<span id="edit-slug-buttons">';
		$xhtml .= '<button type="button" class="edit-slug btn btn-primary btn-sm">Chỉnh sửa</button>';
		$xhtml .= '</span>';
		$xhtml .= '</div>';

        return $xhtml;
    }
}




