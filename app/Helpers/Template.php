<?php 
namespace App\Helpers;
use Illuminate\Support\Facades\Config;
use Collective\Html\FormFacade as Form;

class Template {
    public static function showButtonFilter ($controllerName, $itemsStatusCount, $currentFilterStatus, $paramsSearch) { // $currentFilterStatus active inactive all
        $xhtml = null;
        $tmplStatus = Config::get('zvn.template.status');

        if (count($itemsStatusCount) > 0) {
            array_unshift($itemsStatusCount , [
                'count'   => array_sum(array_column($itemsStatusCount, 'count')),
                'status'  => 'all'
            ]);

            foreach ($itemsStatusCount as $item) {  // $item = [count,status]
                $statusValue = $item['status'];  // active inactive block
                $statusValue = array_key_exists($statusValue, $tmplStatus ) ? $statusValue : 'default';

                $currentTemplateStatus = $tmplStatus[$statusValue]; // $value['status'] inactive block active
                $link = route($controllerName) . "?filter_status=" .  $statusValue;

                if($paramsSearch['value'] !== ''){
                    $link .= "&search_field=" . $paramsSearch['field'] . "&search_value=" .  $paramsSearch['value'];
                }

                $class  = ($currentFilterStatus == $statusValue) ? 'btn-danger' : 'btn-info';
                $xhtml  .= sprintf('<a href="%s" type="button" class="btn %s">
                                    %s <span class="badge bg-white">%s</span>
                                </a>', $link, $class, $currentTemplateStatus['name'], $item['count']);
            }
        }

        return $xhtml;
    }

    public static function showAreaSearch ($controllerName, $paramsSearch) { 
        $xhtml = null;
        $tmplField         = Config::get('zvn.template.search');
        $fieldInController = Config::get('zvn.config.search');

        $controllerName = (array_key_exists($controllerName, $fieldInController)) ? $controllerName : 'default';
        $xhtmlField = null;

        foreach($fieldInController[$controllerName] as $field)  {// all id
            $xhtmlField .= sprintf('<li><a href="#" class="select-field" data-field="%s">%s</a></li>', $field, $tmplField[$field]['name']);
        }
       
        $searchField = (in_array($paramsSearch['field'],  $fieldInController[$controllerName] )) ? $paramsSearch['field'] : "all";

        $xhtml = sprintf('
            <div class="input-group">
                <div class="input-group-btn">
                    <button type="button" class="btn btn-default dropdown-toggle btn-active-field" data-toggle="dropdown" aria-expanded="false">
                        %s <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">
                        %s
                    </ul>
                </div>
                <input type="text" name="search_value" value="%s" class="form-control" >
                <input type="hidden" name="search_field" value="%s">
                <span class="input-group-btn">
                    <button id="btn-clear-search" type="button" class="btn btn-success" style="margin-right: 0px">Xóa tìm kiếm</button>
                    <button id="btn-search" type="button" class="btn btn-primary">Tìm kiếm</button>
                </span>
            </div>', $tmplField[$searchField]['name'], $xhtmlField, $paramsSearch['value'], $searchField);
        return $xhtml;
    }

    public static function showItemHistory ($by, $time) {
        $xhtml = sprintf(
            '<p><i class="fa fa-user"></i> %s</p>
            <p><i class="fa fa-clock-o"></i> %s</p>', $by, date(Config::get('zvn.format.short_time'), strtotime($time)) );
        return $xhtml;
    }

    public static function showItemStatus ($controllerName, $id, $statusValue) {
        $tmplStatus = Config::get('zvn.template.status');
        $statusValue        = array_key_exists($statusValue, $tmplStatus ) ? $statusValue : 'default';
        $currentTemplateStatus = $tmplStatus[$statusValue];
        $link          = route($controllerName . '/status', ['status' => $statusValue, 'id' => $id]);

        $xhtml = sprintf(
            '<a href="%s" type="button" class="btn btn-round %s">%s</a>', $link , $currentTemplateStatus['class'], $currentTemplateStatus['name']  );
        return $xhtml;
    }

    public static function showItemIsHome ($controllerName, $id, $isHomeValue) {
        $tmplIsHome = Config::get('zvn.template.is_home');
        $isHomeValue        = array_key_exists($isHomeValue, $tmplIsHome ) ? $isHomeValue : 'yes';
        $currentTemplateIsHome = $tmplIsHome[$isHomeValue];
        $link          = route($controllerName . '/isHome', ['is_home' => $isHomeValue, 'id' => $id]);

        $xhtml = sprintf(
            '<a href="%s" type="button" class="btn btn-round %s">%s</a>', $link , $currentTemplateIsHome['class'], $currentTemplateIsHome['name']  );
        return $xhtml;
    }

    public static function showItemSelect($controllerName, $id, $displayValue, $fieldName)
    {
       $link          = route($controllerName . '/' . $fieldName, [$fieldName => 'value_new', 'id' => $id]);
        
       $tmplDisplay = Config::get('zvn.template.' . $fieldName);
       $xhtml = sprintf('<select name="select_change_attr" data-url="%s" class="form-control">', $link  );

        foreach ($tmplDisplay as $key => $value) {
           $xhtmlSelected = '';
           if ($key == $displayValue) $xhtmlSelected = 'selected="selected"';
            $xhtml .= sprintf('<option value="%s" %s>%s</option>', $key, $xhtmlSelected, $value['name']);
        }
        $xhtml .= '</select>';

        return $xhtml;
    }
    
    public static function showItemSelectAjax($controllerName, $id, $displayValue, $fieldName, $doAction)
    {
       $link          = route($controllerName . '/ajax', [ 'do' => $doAction, $fieldName => 'value_new', 'id' => $id]);
        
       $tmplDisplay = Config::get('zvn.template.' . $fieldName);
       $xhtml = sprintf('<select name="select_change_attr_ajax" data-url="%s" class="form-control">', $link  );

        foreach ($tmplDisplay as $key => $value) {
           $xhtmlSelected = '';
           if ($key == $displayValue) $xhtmlSelected = 'selected="selected"';
            $xhtml .= sprintf('<option value="%s" %s>%s</option>', $key, $xhtmlSelected, $value['name']);
        }
        $xhtml .= '</select>';

        return $xhtml;
    }
    
    public static function showItemSelect2($controllerName, $id, $data_list = array(), $displayValue, $fieldName, $doAction)
    {
       $link          = route($controllerName . '/ajax', [ 'do' => $doAction, $fieldName => 'value_new', 'id' => $id]);    
       $xhtml = '';
       if(!empty($data_list))
       {
            $xhtml .= sprintf('<select name="select_change_attr_ajax" data-url="%s" class="form-control">', $link  );
            $xhtml .= sprintf('<option value="">%s</option>', ' -- Chọn danh mục --');
            foreach ($data_list as $key => $value) {
                $xhtmlSelected = '';
                if ($key == $displayValue) $xhtmlSelected = 'selected="selected"';
                $xhtml .= sprintf('<option value="%s" %s>%s</option>', $key, $xhtmlSelected, $value);
            }
            $xhtml .= '</select>';
       }
       return $xhtml;
    }

    public static function showListRadio($name = 'target', $arrayList = array(), $active = null)
    {
        $xhtml = '';
        foreach ($arrayList as $key => $val)
        {
            $radio = Form::radio($name, $key, $active == $key ? true : false);
            $xhtml .= sprintf('<div class="radio radio-inline">
                                    <label> %s <i class="helper"></i>%s </label>
                                </div>', $radio, $val['name']
            );
        }
        return $xhtml;
    }

    public static function showRadio($name, $value, $title = null, $active = true)
    {
        $xhtml = null;
        $radio = Form::radio($name, $value, $active);
        $xhtml = sprintf('<div class="radio radio-inline">
                                <label> %s <i class="helper"></i>%s </label>
                            </div>', $radio, $title
        );
        return $xhtml;
    }

    public static function showItemThumb ($controllerName, $thumbName, $thumbAlt) {
        $xhtml = sprintf(
            '<img src="%s" alt="%s" class="zvn-thumb">', (!strstr($thumbName,'http://') && !strstr($thumbName,'https://')) ? asset($thumbName) : $thumbName , $thumbAlt );
        return $xhtml;
    }

    public static function showButtonAction ($controllerName, $id) {
        $tmplButton   = Config::get('zvn.template.button');
        $buttonInArea = Config::get('zvn.config.button');

        $controllerName = (array_key_exists($controllerName, $buttonInArea)) ? $controllerName : "default";
        $listButtons    = $buttonInArea[$controllerName]; // ['edit', 'delete']

        $xhtml = '<div class="zvn-box-btn-filter">';

        foreach ($listButtons as $btn) {
            $currentButton = $tmplButton[$btn];

            $link = route($controllerName . $currentButton['route-name'], ['id' => $id] );
            $xhtml .= sprintf(
                '<a href="%s" type="button" class="btn btn-icon %s" data-toggle="tooltip" data-placement="top" 
                    data-original-title="%s">
                    <i class="fa %s"></i>
                </a>', $link, $currentButton['class'], $currentButton['title'], $currentButton['icon']);
        }

        $xhtml .= '</div>';

        return $xhtml;
    }
    
    public static function showBtnOrdering ($controllerName, $items, $id, $options = []) {
        $tmplButtonOrdering   = Config::get('zvn.template.ordering');

        if(isset($options['task']) && $options['task'] == 'ordering')
        {
            $nodeCurrent = null;
            foreach ( $items as $key => $value) {
                if($value['id'] == $id)
                {
                    $nodeCurrent = $value;
                }
            }
            $nodeNext = $nodeCurrent->getNextSibling(); 
            // var_dump($nodeNext);
            $nodePrev = $nodeCurrent->getPrevSibling();
            // var_dump($nodePrev); die();

            $siblings = $nodeCurrent->getSiblings();
            // var_dump($nodeCurrent['id'] .' - '. $siblings[0]['id']. ' - ' .$siblings[0]['name']);

            $listButtons = [];
            if($nodeNext !== null && $nodePrev !== null)
            {
                $listButtons = ['up','down'];
            }
            elseif($nodeNext !== null && $nodePrev === null)
            {
                $listButtons = ['down'];
            }else{
                $listButtons = ['up'];
            }

            $xhtml = '<div class="zvn-box-btn-filter">';
            foreach ($listButtons as $btn_action) {
                $currentButton = $tmplButtonOrdering[$btn_action];

                $link = route($controllerName .'/order', ['order' => $btn_action, 'id' => $id] );

                $xhtml .= sprintf('<a href="%s" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" 
                        data-original-title="%s">
                        <i class="fa %s"></i>
                    </a>', $link, $currentButton['name'], $currentButton['icon']);
            }
            $xhtml .= '</div>';
        }


        return $xhtml;
    }

    public static function showDatetimeFrontend($dateTime)
    {
        return date_format(date_create($dateTime), Config::get('zvn.format.short_time'));
    }

    public static function showContent($content, $length, $prefix = '...')
    {
        $prefix = ($length == 0) ? '' : $prefix;
        $content = str_replace(['<p>', '</p>'], '', $content);
        return preg_replace('/\s+?(\S+)?$/', '', substr($content, 0, $length)) . $prefix;
    }
   
    public static function showListNetwork($data = array())
    {
        $xhtml = '';
        if(!empty($data))
        {
            $xhtml .= '<ul>';
            foreach ($data as $key => $value) {
                if(!empty($value))
                    $xhtml .= sprintf('<li><strong>%s :</strong> <a href="%s">%s</a></li>', ucfirst($key), url($value), $value);
            }
            $xhtml .= '</ul>';
        }
        
        return $xhtml;
    }
    
    public static function showListSocial($data = array())
    {
        $xhtml = '';
        if(!empty($data))
        {
            $xhtml .= '<ul class="social">';
            foreach ($data as $key => $value) {
                if(!empty($value))
                    $xhtml .= sprintf('<li><a href="%s"><i class="%s"></i></a></li>', url($value['link']), $value['icon']);
            }
            $xhtml .= '</ul>';
        }
        
        return $xhtml;
    }
}
