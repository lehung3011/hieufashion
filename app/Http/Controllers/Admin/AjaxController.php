<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminController;

class AjaxController extends AdminController
{
    
    public function __construct()
    {
        $this->pathViewController = 'admin.pages.dashboard.';  // slider
        $this->controllerName     = 'ajax';

        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {
        $action     = $request->do;
        $jsout      = '';
        switch($action)
		{
            case 'upload_images': 
                $jsout = self::images_upload($request); 
            break; 
			default:
				$jsout = 'Error';
			break;
			}
		flush();
		echo json_encode($jsout);
		exit;
    }

    public function images_upload(Request $request)
    {
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        dd($imageName);
        $image->move(public_path('images'),$imageName);
        
        $imageUpload = new ImageUpload();
        $imageUpload->filename = $imageName;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }
  
}

