<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminController;
use App\Models\ArticleModel as MainModel;
use App\Models\CategoryNewsModel;
use App\Http\Requests\ArticleRequest as MainRequest ;    

class ArticleController extends AdminController
{
    
    public function __construct()
    {
        $this->pathViewController = 'admin.pages.article.';  // slider
        $this->controllerName     = 'article';

        $this->model = new MainModel();
        $this->params["pagination"]["totalItemsPerPage"] = 20;
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        $this->params['filter']['status'] = $request->input('filter_status', 'all' ) ;
        $this->params['search']['field']  = $request->input('search_field', '' ) ; // all id description
        $this->params['search']['value']  = $request->input('search_value', '' ) ;

        $items              = $this->model->listItems($this->params, ['task'  => 'admin-list-items']);
        $itemsStatusCount   = $this->model->countItems($this->params, ['task' => 'admin-count-items-group-by-status']); // [ ['status', 'count']]

        $categoryModel  = new CategoryNewsModel();
        $itemsCategory  = $categoryModel->listItems(null, ['task' => 'admin-list-items-in-selectbox']);

        return view($this->pathViewController .  'index', [
            'params'            => $this->params,
            'items'             => $items,
            'itemsStatusCount'  =>  $itemsStatusCount,
            'itemsCategory'     =>$itemsCategory
        ]);
    }

    public function form(Request $request)
    {
        $item = null;
        if($request->id !== null ) {
            $params["id"] = $request->id;
            $item = $this->model->getItem( $params, ['task' => 'get-item']);
        }

        $categoryModel  = new CategoryNewsModel();
        $itemsCategory  = $categoryModel->listItems(null, ['task' => 'admin-list-items-in-selectbox']);

        return view($this->pathViewController .  'form', [
            'item'        => $item,
            'itemsCategory'=>$itemsCategory
        ]);
    }

    public function save(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $params = $request->all();
            
            $task   = "add-item";
            $notify = "Thêm phần tử thành công!";

            if($params['id'] !== null) {
                $task   = "edit-item";
                $notify = "Cập nhật phần tử thành công!";
            }
            $this->model->saveItem($params, ['task' => $task]);
            return redirect()->route($this->controllerName)->with("zvn_notify", $notify);
        }
    }

    public function ajax(Request $request)
    {
        $action     = $request->do;
        $jsout      = '';
		switch($action) 
		{	
            case 'change-category':
                $params["category"]     = $request->category;
                $params["id"]           = $request->id;
                $jsout = $this->model->saveItem($params, ['task' => 'change-category']); 
			break;
		}
		flush();
		echo json_encode($jsout);
		exit();
    }
    
}