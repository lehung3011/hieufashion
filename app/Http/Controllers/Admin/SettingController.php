<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\Admin\AdminController;
use App\Models\SettingModel as MainModel;
use App\Http\Requests\SettingRequest as MainRequest ;    

class SettingController extends AdminController
{
    
    public function __construct() 
    {
        $this->pathViewController = 'admin.pages.setting.'; 
        $this->controllerName     = 'setting';

        $this->model = new MainModel();
        $this->params["pagination"]["totalItemsPerPage"] = 5;
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        return view($this->pathViewController .  'index');
    }

    public function save(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $params = $request->all();

            $task   = "edit-item";
            $notify = "Cập nhật phần tử thành công!";

            switch ($params['key']) {
                case 'general':
                    $key_value = 'setting-main';
                    break;
                default:
                    $key_value = 'setting-'.$params['key'];
                    break;
            }
            
            $this->model->saveItem($params, ['task' => $task , 'key_value' => $key_value]); 
            return redirect()->route($this->controllerName.'/'.$params['key'])->with("zvn_notify", $notify);
        }
    }

    public function general(Request $request)
    {
        $key = __FUNCTION__;
        $this->params['key'] = 'setting-main';
        $item    = $this->model->getItem($this->params, ['task'  => 'get-setting']);
        $setting = json_decode($item['value'], true);
        return view($this->pathViewController .  'general', compact( 'key', 'setting' ) );
    }
    
    public function email(Request $request)
    {
        $key = __FUNCTION__;
        $this->params['key'] = 'setting-'.$key;
        $item    = $this->model->getItem($this->params, ['task'  => 'get-setting']);
        $setting = json_decode($item['value'], true);

        // $tags = Links::existingTags()->pluck('name');

        return view($this->pathViewController .  'email', compact( 'key', 'setting' ) );
    }
    
    public function social(Request $request)
    {
        $key = __FUNCTION__;
        $this->params['key'] = 'setting-'.$key;
        $item    = $this->model->getItem($this->params, ['task'  => 'get-setting']);
        $setting = json_decode($item['value'], true);
        return view($this->pathViewController .  'social', compact( 'key', 'setting' ) );
    }
    
    public function chat(Request $request)
    {
        $key = __FUNCTION__;
        $this->params['key'] = 'setting-'.$key;
        $item    = $this->model->getItem($this->params, ['task'  => 'get-setting']);
        $setting = json_decode($item['value'], true);
        return view($this->pathViewController .  'chat', compact( 'key', 'setting' ) );
    }
    

   

    public function ajax(Request $request)
    {
        $action     = $request->do;
        $jsout      = '';
		switch($action) 
		{	
            case 'change-level':
                $params["currentLevel"]   = $request->level;
                $params["id"]             = $request->id;
                $jsout = $this->model->saveItem($params, ['task' => 'change-level']);
			break;
		}
		flush();
		echo json_encode($jsout);
		exit();
    }

}