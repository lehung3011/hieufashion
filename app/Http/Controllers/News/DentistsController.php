<?php

namespace App\Http\Controllers\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;;    

use App\Models\DentistsModel;
use App\Models\CategoryModel;

class DentistsController extends Controller
{
    private $pathViewController = 'news.pages.dentists.';  // slider
    private $controllerName     = 'dentists';
    private $params             = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {     
        $params["dentists_id"]  = $request->dentists_id;
        $dentistsModel  = new DentistsModel();
        

        $itemDentists = $dentistsModel->getItem($params, ['task' => 'news-get-item']);
        if(empty($itemDentists))  return redirect()->route('home');
        
        $itemsLatest   = $dentistsModel->listItems(null, ['task'  => 'news-list-items-latest']);
        
        $params["category_id"]  = $itemDentists['category_id'];
        $itemDentists['related_dentistss'] = $dentistsModel->listItems($params, ['task' => 'news-list-items-related-in-category']);
       
        return view($this->pathViewController .  'index', [
            'params'        => $this->params,
            'itemsLatest'   => $itemsLatest,
            'itemDentists'  => $itemDentists
        ]);
    }
 
}