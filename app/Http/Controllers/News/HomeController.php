<?php

namespace App\Http\Controllers\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;;    

use App\Models\SliderModel;
use App\Models\ArticleModel;
use App\Models\CategoryModel;
use App\Models\CommentsModel;
use App\Models\AgenciesModel;
use App\Models\RecallModel;
use App\Models\SettingModel;

class HomeController extends Controller
{
    private $pathViewController = 'news.pages.home.';  // slider
    private $controllerName     = 'home';
    private $params             = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function index(Request $request)
    {   
        $sliderModel   = new SliderModel();
        $categoryModel = new CategoryModel();
        $articleModel  = new ArticleModel();
        $commentsModel  = new CommentsModel();
        $agenciesModel  = new AgenciesModel();

        $params = null;

        $itemsSlider   = $sliderModel->listItems(null, ['task'   => 'news-list-items']);
        $itemsPromotion = $articleModel->listItems(['category_id' => 1], ['task'  => 'news-list-items-in-category']); 
        $itemsLatest   = $articleModel->listItems(null, ['task'  => 'news-list-items-latest']);
        $itemsComment  = $commentsModel->listItems(null, ['task'  => 'news-list-items']);
            
        $listDistrict = config('zvn.template.district');
        $itemsAgencies['Tất cả'] = $agenciesModel->listItems(null, ['task'  => 'news-list-items']);
        foreach ($listDistrict as $district) {
            $list = $agenciesModel->listItems(['district' => $district], ['task'  => 'news-list-items-by-district']);
            if(!empty($list))
            {
                $itemsAgencies[$district]  = $list;
            }
        }
            
        return view($this->pathViewController .  'index', compact(
            'params', 'itemsSlider' , 'itemsPromotion', 'itemsLatest' ,'itemsComment', 'itemsAgencies'
        ));
    }

    public function getphone(Request $request)
    {
        $jsout = array();
        if($request->ajax())
        {
            $phone = $request->phone;

            $recallModel  = new RecallModel();
            $task   = "add-item";
            $params['phone'] = $phone;
            $params['status'] = 'inactive';
            $recallModel->saveItem($params, ['task' => $task]);

            // Send mail
            $email_received = get_json_setting('setting-email', 'bcc_guest_recall');
            if(empty($email_received))
            {
                $settingModel = new SettingModel();
                $setting_mail = $settingModel->getItem(['key' => 'setting-email'], ['task' => 'get-setting']);
                $email_received = json_decode($setting_mail['value'], true)['bcc_guest_recall'];
            }
            $emails = explode(',', $email_received);
            $title = "Nha Khoa Lutadent - Yêu cầu gọi lại";
            $content = "<h3>Bạn nhận được yêu cầu gọi lại tư vấn từ khách hàng</h3>\n
            <ul>
                <li>Điện thoại : ".$phone."</li>
                <li>Ngày yêu cầu : ".date('d/m/Y H:i', time())."</li>
            </ul>";

            foreach ($emails as $key => $email) {
                send_mail($email, $title, $content, []);
            }


            $jsout['code'] = 1;
        }else{
            die('ERROR !!!');
        }
		
		flush();
		echo json_encode($jsout);
		exit();
    }
 
}