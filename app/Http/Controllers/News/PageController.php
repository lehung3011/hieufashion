<?php

namespace App\Http\Controllers\News;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;   

class PageController extends Controller
{
    private $pathViewController = 'news.pages.page.';  // slider
    private $controllerName     = 'page';
    private $params             = [];
    private $model;

    public function __construct()
    {
        view()->share('controllerName', $this->controllerName);
    }

    public function notification(Request $request)
    {   
        $params = null;    
        return view($this->pathViewController .  'notification');
    }
 
}