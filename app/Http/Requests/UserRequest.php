<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\IsCurrentPassword;

class UserRequest extends FormRequest
{
    private $table            = 'user';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;
        $task   = $this->task;

        $condAvatar   = '';
        $condUserName = '';
        $condEmail    = '';
        $condPass     = '';
        $condLevel    = '';
        $condStatus   = '';
        $condFullname = '';

       
        switch ($task) {
            case 'add':
                $condUserName   = "bail|required|between:5,100|unique:$this->table,username";
                $condEmail      = "bail|required|email|unique:$this->table,email";
                $condFullname   = 'bail|required|min: 5';
                $condPass       = 'bail|required|between:5,100|confirmed';
                $condStatus     = 'bail|in:active,inactive';
                $condLevel      = 'bail|in:admin,member';
                $condAvatar     = 'bail|required|image|max:500';
                break;
            case 'edit-info':
                $condUserName   = "bail|required|between:5,100|unique:$this->table,username,$id"; 
                $condFullname   = 'bail|required|min: 5';
                $condAvatar     = 'bail|image|max:500';
                $condStatus     = 'bail|in:active,inactive';
                $condEmail      = "bail|required|email|unique:$this->table,email,$id";
                break;
            case 'change-password':
                $condPass = 'bail|required|between:5,100|confirmed';
                break;
            case 'change-password-2':
                $condOldPass    = ['required', new IsCurrentPassword];
                $condNewPass    = 'bail|required|between:5,100|confirmed';
                break;
            case 'change-level':
                $condLevel = 'bail|in:admin,member';
                break;
            default:
                break;
        }
        
        switch ($task) {
            case 'change-password-2':
                return [
                    'old_password' => $condOldPass,
                    'password'    => $condNewPass
                ];
            break;
            default: 
                return [
                    'username' => $condUserName,
                    'email'    => $condEmail,
                    'fullname' => $condFullname,
                    'status'   => $condStatus,
                    'password' => $condPass,
                    'level'    => $condLevel,
                    'avatar'   => $condAvatar
                ];
            break;
        }
        
    }

    public function messages()
    {
        $task   = $this->task;
        switch ($task) {
            case 'change-password-2':
                return [
                    'old_password.required'     => 'Vui lòng nhập mật khẩu cũ',
                    'old_password.between'      => 'Mật khẩu từ 5 => 100 ký tự',
                    'password.required'     => 'Vui lòng nhập mật khẩu mới',
                    'password.between'      => 'Mật khẩu từ 5 => 100 ký tự',
                    'password_confirmation.required'     => 'Vui lòng xác nhận lại nhập mật.',
                ];
            break;
            default:
                return [
                    // 'name.required' => 'Name không được rỗng',
                    // 'name.min'      => 'Name :input chiều dài phải có ít nhất :min ký tứ',
                ];
            break;
        }
        
    }
    public function attributes()
    {
        return [
            // 'description' => 'Field Description: ',
        ];
    }
}
