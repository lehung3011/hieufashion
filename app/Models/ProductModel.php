<?php

namespace App\Models;

use App\Models\AdminModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use DB; 
class ProductModel extends AdminModel
{

    public function __construct() {
        $this->table               = 'product as a';
        $this->folderUpload        = 'product' ; 
        $this->fieldSearchAccepted = ['title', 'description']; 
        $this->crudNotAccepted     = ['_token','picture_current', 'id'];
    }

    public function listItems($params = null, $options = null) {
     
        $result = null;

        if($options['task'] == "admin-list-items") {
            $query = $this->select('a.id', 'a.title', 'a.status', 'a.description', 'a.picture', 'c.id as category_id','c.name as category_name')
                          ->leftJoin('cate_product as c', 'a.cat_id', '=', 'c.id');


            if ($params['filter']['status'] !== "all")  {
                $query->where('a.status', '=', $params['filter']['status'] );
            }

            if ($params['search']['value'] !== "")  {
                if($params['search']['field'] == "all") {
                    $query->where(function($query) use ($params){
                        foreach($this->fieldSearchAccepted as $column){
                            $query->orWhere('a.' . $column, 'LIKE',  "%{$params['search']['value']}%" );
                        }
                    });
                } else if(in_array($params['search']['field'], $this->fieldSearchAccepted)) { 
                    $query->where('a.' . $params['search']['field'], 'LIKE',  "%{$params['search']['value']}%" );
                } 
            }

            $result =  $query->orderBy('a.id', 'desc')
                            ->paginate($params['pagination']['totalItemsPerPage']);

        }

        if($options['task'] == "admin-list-items-select") {
            $query = $this->select('id', 'title')
                        ->where('status', '=', 'active' );

            $result = $query->get()->toArray();
        }

        if($options['task'] == 'news-list-items') {
            $query = $this->select('id', 'title', 'picture')
                        ->where('status', '=', 'active' )
                        ->limit(5);

            $result = $query->get()->toArray();
        }

        if($options['task'] == 'news-list-items-featured') {
	
            $query = $this->select('a.id', 'a.title', 'a.description', 'a.created', 'a.cat_id', 'c.name as category_name', 'a.picture')
                ->leftJoin('cate_product as c', 'a.cat_id', '=', 'c.id')
                ->where('a.status', '=', 'active')
                ->where('a.type', 'featured')
                ->orderBy('a.id', 'desc')
                ->take(3);

            $result = $query->get()->toArray();
        }

        if($options['task'] == 'news-list-items-latest') {
            
            $query = $this->select('a.id', 'a.title', 'a.created', 'a.cat_id', 'c.name as category_name', 'a.picture')
                ->leftJoin('cate_product as c', 'a.cat_id', '=', 'c.id')
                ->where('a.status', '=', 'active')
                ->orderBy('id', 'desc') 
                ->take(4);
            ;
            $result = $query->get()->toArray();
        }

        if($options['task'] == 'news-list-items-in-category') {
            $query = $this->select('id', 'title', 'description', 'picture', 'created')
                ->where('status', '=', 'active')
                ->where('cat_id', '=', $params['category_id'])
                ->take(4)
            ;
            $result = $query->get()->toArray();
        }
        
        if($options['task'] == 'news-list-items-related-in-category') {
            $query = $this->select('id', 'title', 'description', 'picture', 'created')
                ->where('status', '=', 'active')
                ->where('a.id', '!=', $params['product_id'])
                ->where('cat_id', '=', $params['category_id'])
            ;
            if (isset($params['limit']))  {
                $query->take($params['limit']);
            }
            else{
                $query->take(4);
            }
            $result = $query->get()->toArray();
        }
        
        return $result;
    }

    public function countItems($params = null, $options  = null) {
     
        $result = null;

        if($options['task'] == 'admin-count-items-group-by-status') {
         
            $query = $this::groupBy('status')
                        ->select( DB::raw('status , COUNT(id) as count') );

            if ($params['search']['value'] !== "")  {
                if($params['search']['field'] == "all") {
                    $query->where(function($query) use ($params){
                        foreach($this->fieldSearchAccepted as $column){
                            $query->orWhere($column, 'LIKE',  "%{$params['search']['value']}%" );
                        }
                    });
                } else if(in_array($params['search']['field'], $this->fieldSearchAccepted)) { 
                    $query->where($params['search']['field'], 'LIKE',  "%{$params['search']['value']}%" );
                } 
            }

            $result = $query->get()->toArray();
           

        }

        return $result;
    }

    public function getItem($params = null, $options = null) { 
        $result = null;
        
        if($options['task'] == 'get-item') {
            $result = self::select('id', 'title', 'description', 'status', 'picture', 'cat_id', 'friendly_title', 'metakey', 'metadesc')->where('id', $params['id'])->first();
        }

        if($options['task'] == 'get-thumb') {
            $result = self::select('id', 'picture')->where('id', $params['id'])->first();
        }

        if($options['task'] == 'news-get-item') {
            $result = self::select('a.id', 'a.title', 'description', 'a.cat_id', 'c.name as category_name', 'a.picture', 'a.created', 'c.display')
                         ->leftJoin('cate_product as c', 'a.cat_id', '=', 'c.id')
                         ->where('a.id', '=', $params['product_id'])
                         ->where('a.status', '=', 'active')->first();
            if($result) $result = $result->toArray();
        }

        if($options['task'] == 'get-by-category-id')
        {
            $query = self::select('a.id', 'a.title')
            ->where('category_id', '=', $params['id'] )
            ->where('status', '=', 'active' )
            ->limit(5);
            $result = $query->get();
        }

        return $result;
    }

    public function saveItem($params = null, $options = null) { 
        if($options['task'] == 'change-status') {
            $status = ($params['currentStatus'] == "active") ? "inactive" : "active";
            self::where('id', $params['id'])->update(['status' => $status ]);
        }

        if($options['task'] == 'change-type') {
            self::where('id', $params['id'])->update(['type' => $params['currentType']]);
        }
        
        if($options['task'] == 'change-category') {
            $upd = array();
            $upd['category_id']   = $params['category'];
            $upd['modified_by']   = session('userInfo')['username'];
            $upd['modified']      = date('Y-m-d H:i');
            return  self::where(['id' => $params['id'] ] )->update($this->prepareParams($upd));
        }
        

        if($options['task'] == 'add-item') {
            $params['friendly_title'] = !empty($params['friendly_title']) ? $params['friendly_title'] : $params['title'];
            $params['friendly_url'] = Str::slug($params['title'], '-'); 
            $params['metakey'] = !empty($params['metakey']) ? $params['metakey'] : null;
            $params['metadesc'] = !empty($params['metadesc']) ? $params['metadesc'] : strip_tags($params['description']);
            $params['metadesc'] = Str::limit($params['metadesc'], 300);

            $params['created_by'] = session('userInfo')['username'];
            $params['created']    = date('Y-m-d H:i');
            DB::table('product')->insertGetId($this->prepareParams($params));     
        }

        if($options['task'] == 'edit-item') {
            $params['modified_by']   = session('userInfo')['username'];
            $params['modified']      = date('Y-m-d H:i');

            self::where(['id' => $params['id'] ] )->update($this->prepareParams($params));
        }
    }

    public function deleteItem($params = null, $options = null) 
    { 
        if($options['task'] == 'delete-item') {
            $item   = self::getItem($params, ['task'=>'get-thumb']);
            $this->deleteThumb($item['picture']);
            self::where('id', $params['id'])->delete();
        }
    }

}

