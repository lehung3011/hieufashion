<?php

namespace App\Models;

use App\Models\AdminModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use \Conner\Tagging\Taggable;   

class SettingModel extends AdminModel
{
    use Taggable;

    public function __construct() {
        $this->table               = 'settings';
        $this->folderUpload        = 'weblink' ;
        $this->fieldSearchAccepted = null; 
        $this->crudNotAccepted     = ['_token', 'logo_current', 'setting', 'key'];
        $this->crudNotAcceptedSaveFile = ['smtp_username', 'smtp_password'];
    }

    public function listItems($params = null, $options = null) {
     
        $result = null;

        if($options['task'] == "admin-list-items") 
        {
            $query = $this->select('id', 'username', 'email', 'fullname', 'avatar', 'status', 'level','created', 'created_by', 'modified', 'modified_by');
               
            if ($params['filter']['status'] !== "all")  {
                $query->where('status', '=', $params['filter']['status'] );
            }

            if ($params['search']['value'] !== "")  {
                if($params['search']['field'] == "all") {
                    $query->where(function($query) use ($params){
                        foreach($this->fieldSearchAccepted as $column){
                            $query->orWhere($column, 'LIKE',  "%{$params['search']['value']}%" );
                        }
                    });
                } else if(in_array($params['search']['field'], $this->fieldSearchAccepted)) { 
                    $query->where($params['search']['field'], 'LIKE',  "%{$params['search']['value']}%" );
                } 
            }

            $result =  $query->orderBy('id', 'desc')
                            ->paginate($params['pagination']['totalItemsPerPage']);

        }

        return $result;
    }

    public function getItem($params = null, $options = null) { 
        $result = null;

        if($options['task'] == 'get-setting') {
            $result = self::select('value')->where('key_value', $params['key'])->first()->toArray();
        }

        return $result;
    }

    public function saveItem($params = null, $options = null) { 

        if($options['task'] == 'edit-item') {

            // Lưu vào file setting
            save_json_setting(array_diff_key($params['setting'], array_flip($this->crudNotAcceptedSaveFile)), $options['key_value']);

            $setting = json_encode($params['setting']);
            $params['value'] = $setting;
            $params['modified_by']   = session('userInfo')['username'];;
            $params['modified']      = date('Y-m-d');
      
            self::where('key_value', $options['key_value'])->update($this->prepareParams($params));
        }
    }

    public function deleteItem($params = null, $options = null) 
    { 
        if($options['task'] == 'delete-item') {
            $item   = self::getItem($params, ['task'=>'get-avatar']); // 
            $this->deleteThumb($item['avatar']);
            self::where('id', $params['id'])->delete();
        }
    }

}

