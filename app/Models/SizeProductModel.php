<?php

namespace App\Models;

use App\Models\AdminModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Kalnoy\Nestedset\NestedSet;
use Kalnoy\Nestedset\NodeTrait;  
use DB; 


class SizeProductModel extends AdminModel
{
    use NodeTrait;
    protected $guarded = ['size_id'];  
    protected $primaryKey = 'size_id';

    public function __construct() {
        $this->table               = 'product_size';
        $this->folderUpload        = 'category' ; 
        $this->fieldSearchAccepted = ['size_id', 'name'];
        $this->crudNotAccepted     = ['_token'];

        // SizeProductModel::fixTree();
    }

    public function listItems($params = null, $options = null) {
     
        $result = null; 

        if($options['task'] == "admin-list-items") {
            $query = $this->select('size_id', 'name', 'status', 'parent_id', '_lft', '_rgt', 'created', 'created_by', 'modified', 'modified_by');
               
            if ($params['filter']['status'] !== "all")  {
                $query->where('status', '=', $params['filter']['status'] );
            }

            if ($params['search']['value'] !== "")  
            {
                if($params['search']['field'] == "all") {
                    $query->where(function($query) use ($params){
                        foreach($this->fieldSearchAccepted as $column){
                            $query->orWhere($column, 'LIKE',  "%{$params['search']['value']}%" );
                        }
                    });
                } else if(in_array($params['search']['field'], $this->fieldSearchAccepted)) { 
                    $query->where($params['search']['field'], 'LIKE',  "%{$params['search']['value']}%" );
                } 
            }

            if(isset($params['select']['field']) && $params['select']['value'] !== 'default')
            {
                $query->where($params['select']['field'], '=',  "{$params['select']['value']}" );
            }
            $result =  $query->defaultOrder()->paginate($params['pagination']['totalItemsPerPage']);
        }

        if($options['task'] == "admin-list-items-in-selectbox") {
            $query = $this->select('size_id', 'name')
                        ->orderBy('name', 'asc')
                        ->where('status', '=', 'active' );
                        
            $result = $query->pluck('name', 'size_id')->toArray();
        
        }

        if($options['task'] == 'news-list-items') {
            $query = $this->select('size_id', 'name')
                        ->where('status', '=', 'active' );
            if($params['limit']){
                $query->limit($params['limit']);
            } 

            $result = $query->get()->toArray();
        }

        return $result;
    }

    public function countItems($params = null, $options  = null) {
     
        $result = null;

        if($options['task'] == 'admin-count-items-group-by-status') {
         
            $query = $this::groupBy('status')
                        ->select( DB::raw('status , COUNT(size_id) as count') );

            if ($params['search']['value'] !== "")  {
                if($params['search']['field'] == "all") {
                    $query->where(function($query) use ($params){
                        foreach($this->fieldSearchAccepted as $column){
                            $query->orWhere($column, 'LIKE',  "%{$params['search']['value']}%" );
                        }
                    });
                } else if(in_array($params['search']['field'], $this->fieldSearchAccepted)) { 
                    $query->where($params['search']['field'], 'LIKE',  "%{$params['search']['value']}%" );
                } 
            }

            $result = $query->get()->toArray();
           

        }

        return $result;
    }

    public function getItem($params = null, $options = null) { 
        $result = null;
        
        if($options['task'] == 'get-item') {
            $result = self::select('size_id', 'name', 'status')->where('size_id', $params['size_id'])->first();
        }

        if($options['task'] == 'news-get-item') {
            $result = self::select('size_id', 'name', 'display')->where('size_id', $params['category_id'])->first();

            if($result) $result = $result->toArray();
        }
        
        return $result;
    }

    public function saveItem($params = null, $options = null) { 

        if($options['task'] == 'change-status') {
            $status = ($params['currentStatus'] == "active") ? "inactive" : "active";
            self::where('size_id', $params['size_id'])->update(['status' => $status ]);
        }

        if($options['task'] == 'change-order')
        {
            $order_change = $params['order'];
            if($order_change == 'down')
            {
                $current = self::find($params['size_id']);
                $current->down();
            }
            else
            {
                $current = self::find($params['size_id']);
                $current->up();
            }
        }

        if($options['task'] == 'add-item') {
            $params['created_by'] = session('userInfo')['username'];
            $params['created']    = date('Y-m-d');
            $parent =  self::find($params['parent_id']); 

            $node = new SizeProductModel();
            foreach ($this->prepareParams($params) as $key => $value) {
                $node->$key = $value;
            }
            // $node->parent()->associate($parent)->save();
            $node->prependToNode($parent)->save();
        }

        if($options['task'] == 'edit-item') 
        {
            $params['modified_by']   = session('userInfo')['username'];
            $params['modified']      = date('Y-m-d');
            // self::where('size_id', $params['size_id'])->update($this->prepareParams($params));
            $parent =  self::find($params['parent_id']);
            $current = self::find($params['size_id']);
            foreach ($this->prepareParams($params) as $key => $value) {
                $current->$key = $value;
            }
            $current->save();
            if($current->parent_id !== $params['parent_id'])
            {
                $current->prependToNode($parent)->save();
            }
        }
    }

    public function deleteItem($params = null, $options = null) 
    { 
        if($options['task'] == 'delete-item') {
            // self::where('size_id', $params['size_id'])->delete(); // Không xóa theo các này làm hư cấu trúc cây
            $current = self::find($params['size_id']);
            $current->delete();
        }
    }

    public static function createSelectMenus()
    {
        $nodes = self::get()->toTree();
        $listMenu = [];
        $traverse = function ($categories, $prefix = '') use (&$traverse, &$listMenu) {
            foreach ($categories as $category) {
                $id = $category->id;
                $name = $prefix.' '.$category->name;
                $listMenu[$id] = $name;
                $traverse($category->children, $prefix.'|-----');
            }
        };
        $traverse($nodes);

        return $listMenu;
    }

}

