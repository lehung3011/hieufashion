<?php

return [
    'opening'          => [
        ['day'   =>  'Thứ 2 - Thứ 7',  'time' => '08:00 - 20:00'], 
        ['day'   =>  'Chủ nhật'     ,  'time' => '08:00 - 17:00'], 
    ],
    'note_datlichhen'       => 'Khi đặt lịch hẹn thành công. Chúng tôi sẽ liên hệ đến quý khách trong thời gian sớm nhất.',
    'email'                 => 'lehung.3011@gmail.com',
    'hotline'               => '02822670999 - 02822460777',
    'social'    =>  [
        'facebook'      =>  [
            'icon'  =>  'fa fa-facebook',
            'link'  =>  'https://www.facebook.com/nhakhoaLutadent/'      
        ],
        'google'        =>  [
            'icon'  =>  'fa fa-google',
            'link'  =>  '' 
        ],
        'twitter'       =>  [
            'icon'  =>  'fa fa-twitter',
            'link'  =>  '' 
        ],
    ]
];