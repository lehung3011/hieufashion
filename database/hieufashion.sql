-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 27, 2020 at 06:16 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hieufashion`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` varchar(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `email`, `fullname`, `password`, `avatar`, `level`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'ltH9r6RUdr.jpeg', 'admin', '2014-12-10 08:55:35', 'admin', '2020-06-12 00:00:00', 'hailan', 'active'),
(2, 'lehung3011', 'lehung.3011@gmail.com', 'Lê Hùng', '6d08f251456f1b9b2538b0fb66223142', 'nuaKQiRFvf.jpeg', 'member', '2020-06-12 00:00:00', 'hailan', NULL, NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friendly_title` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `friendly_url` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metakey` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `category_id`, `name`, `content`, `status`, `thumb`, `type`, `friendly_title`, `friendly_url`, `metakey`, `metadesc`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 2, 'Vienne Tran - Thương hiệu mới của Lamer Fashion', '<div class=\"info-description-article clearfix\">\r\n<p style=\"text-align: justify;\">Với mong muốn đ&aacute;p ứng nhu cầu đa dạng của nhiều nh&oacute;m đối tượng, Lamer Fashion đ&atilde; ph&aacute;t triển một thương hiệu thời trang mới -&nbsp;<strong>Vienne Tran Design</strong>.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">Vienne Tran l&agrave; thương hiệu đầu ti&ecirc;n trong chiến lược ứng dụng m&ocirc; h&igrave;nh \"<em><strong>shop in shop</strong></em>\" của Lamer Fashion, d&agrave;nh ri&ecirc;ng cho ph&acirc;n kh&uacute;c c&ocirc;ng sở c&oacute; độ tuổi từ 25 đến 30 tuổi. Trong tương lai, Lamer sẽ tiếp tục đầu tư v&agrave; nh&acirc;n rộng m&ocirc; h&igrave;nh n&agrave;y với những thương hiệu phục vụ nhiều ph&acirc;n kh&uacute;c kh&aacute;c nhau để tối ưu h&oacute;a trải nghiệm v&agrave; lựa chọn cho kh&aacute;ch h&agrave;ng.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000178779/file/thuong_hieu_cua_l_mer1_grande.png\" /></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 10pt;\" data-sheets-value=\"{\" data-sheets-userformat=\"{\"><em>Vienne Tran Design - Thương hiệu mới của Lamer Fashion</em></span></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">C&aacute;c thiết kế của Vienne Tran giữ tinh thần của hai phong c&aacute;ch chủ đạo đ&oacute; l&agrave;&nbsp;<strong>minimalism</strong>&nbsp;v&agrave;&nbsp;<strong>smart casual</strong>.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\"><strong>Minimalism</strong>&nbsp;l&agrave; phong c&aacute;ch thời trang tối giản, loại bỏ tối đa c&aacute;c chi tiết thừa, kh&ocirc;ng cần thiết tr&ecirc;n bộ trang phục m&agrave; tr&ocirc;ng vẫn thanh lịch, thời thượng. Đặc biệt những bộ trang phục mang phong c&aacute;ch minimalism gần như kh&ocirc;ng bao giờ lỗi mốt hay tạo cảm gi&aacute;c nh&agrave;m ch&aacute;n nhờ t&iacute;nh chất \"basic\", đơn giản vốn c&oacute;.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: center;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000178779/file/v63f18t037-rop14__2__copy_grande.jpg\" /></p>\r\n<p style=\"text-align: center;\"><em>Một sản phẩm của Vienne Tran ti&ecirc;u biểu cho phong c&aacute;ch minimalism</em></p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p style=\"text-align: center;\"><img src=\"//file.hstatic.net/1000178779/file/v61h18q003-ves15_l62m18t007-s1400__2__grande.jpg\" /></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 10pt;\"><em>Đen - trắng, gam m&agrave;u \"huyền thoại\" kh&ocirc;ng bao giờ lỗi mốt</em></span></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: center;\"><img src=\"//file.hstatic.net/1000178779/file/v62t18t009-rds41__v61w18t042-wos15__1__copy_grande.jpg\" /></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 10pt;\"><em>Những gam m&agrave;u đơn sắc vừa thanh lịch, vừa tinh tế</em></span></p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">C&ograve;n&nbsp;<strong>smart casual&nbsp;</strong>l&agrave; một phong c&aacute;ch kh&aacute; quen thuộc được giới mộ điệu thời trang thế giới n&oacute;i chung v&agrave; ch&acirc;u &Acirc;u n&oacute;i ri&ecirc;ng rất ưa chuộng bởi sự tiện lợi. Những bộ trang phục smart casual c&oacute; thể được mặc trong nhiều trường hợp như vừa đi l&agrave;m, vừa đi chơi nhưng vẫn đảm bảo sự chỉn chu, thanh lịch.</p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"//file.hstatic.net/1000178779/file/smart_casual_grande.png\" /></p>\r\n<p style=\"text-align: center;\"><span data-sheets-value=\"{\" data-sheets-userformat=\"{\"><span style=\"font-size: 10pt;\"><em>Smart casual - một phong c&aacute;ch được ưa chuộng bởi sự tiện lợi</em></span><br /></span></p>\r\n<p style=\"text-align: center;\">&nbsp;</p>\r\n<p style=\"text-align: center;\"><img src=\"//file.hstatic.net/1000178779/file/v61b18t031-nos26__v64k18q002-rpp15__2__grande.jpg\" /></p>\r\n<p style=\"text-align: center;\"><span style=\"font-size: 10pt;\"><em>Chiếc &aacute;o \"tưởng 2 m&agrave; 1\" thiết kế bởi Vienne Tran Design</em></span></p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\"><span data-sheets-value=\"{\" data-sheets-userformat=\"{\">Với sự ph&aacute;t triển kh&ocirc;ng ngừng v&agrave; nhu cầu mua sắm ng&agrave;y c&agrave;ng tăng cao của c&aacute;c chị em, ch&uacute;ng t&ocirc;i hy vọng Vienne Tran sẽ mang đến một l&agrave;n s&oacute;ng thời trang mới, để chị em c&oacute; th&ecirc;m lựa chọn về mẫu m&atilde;, kiểu d&aacute;ng cũng như phong c&aacute;ch của ri&ecirc;ng m&igrave;nh.</span></p>\r\n<p style=\"text-align: justify;\"><span data-sheets-value=\"{\" data-sheets-userformat=\"{\"><br />C&ugrave;ng ch&uacute;ng t&ocirc;i kh&aacute;m ph&aacute; những sản phẩm của&nbsp;<strong>Vienne Tran</strong>&nbsp;tại</span></p>\r\n<p style=\"text-align: justify;\"><em><strong>Website:</strong>&nbsp;http://viennetran.vn/</em></p>\r\n<p style=\"text-align: justify;\"><em><strong>Fanpage facebook:</strong>&nbsp;https://www.facebook.com/viennetrandesign/</em></p>\r\n</div>\r\n<div class=\"info-author-article\" style=\"text-align: right;\"><strong>Nguyễn Kh&aacute;nh Linh</strong></div>', 'active', '/uploads/article/test.jpg', NULL, 'Vienne Tran - Thương hiệu mới của Lamer Fashion', 'vienne-tran-thuong-hieu-moi-cua-lamer-fashion', NULL, '\r\nVới mong muốn đ&aacute;p ứng nhu cầu đa dạng của nhiều nh&oacute;m đối tượng, Lamer Fashion đ&atilde; ph&aacute;t triển một thương hiệu thời trang mới -&nbsp;Vienne Tran Design.\r\n&nbsp;\r\nVienne Tran l&agrave; thương hiệu đầu ti&ecirc;n trong chiến lược ứng dụng m&ocirc; h&igrave;nh \"shop in shop\"...', '2020-07-16 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `_lft` int(11) NOT NULL DEFAULT '0',
  `_rgt` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `status` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `is_home` text,
  `display` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_id`, `_lft`, `_rgt`, `name`, `status`, `created`, `created_by`, `modified`, `modified_by`, `is_home`, `display`) VALUES
(1, NULL, 1, 2, 'Ưu đãi', 'active', '2019-05-04 00:00:00', 'admin', '2019-07-09 09:41:27', 'admin', 'yes', 'list'),
(2, NULL, 3, 4, 'Bảng giá', 'active', '2019-05-04 00:00:00', 'admin', '2019-07-09 08:42:58', 'admin', 'yes', 'list'),
(3, NULL, 5, 6, 'Dịch vụ', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-15 15:04:33', 'admin', 'yes', 'list'),
(4, NULL, 7, 8, 'Khách hàng', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-15 15:04:30', 'admin', 'yes', 'list'),
(5, NULL, 9, 10, 'Chăm sóc răng cơ bản', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-12 00:00:00', 'admin', 'yes', 'list'),
(6, NULL, 11, 12, 'Chăm sóc răng cho bé', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-15 15:04:38', 'admin', 'yes', 'grid'),
(7, NULL, 13, 14, 'Chăm sóc răng cho mẹ', 'active', '2019-05-04 00:00:00', 'admin', '2019-05-15 15:04:36', 'admin', 'yes', 'list'),
(8, NULL, 15, 16, 'Chăm sóc răng người lớn tuổi', 'active', '2019-05-12 00:00:00', 'admin', '2020-06-12 00:00:00', 'hailan', 'yes', 'list'),
(9, NULL, 17, 18, 'Giới thiệu', 'active', '2020-06-12 00:00:00', 'hailan', NULL, NULL, 'yes', 'list');

-- --------------------------------------------------------

--
-- Table structure for table `cate_news`
--

DROP TABLE IF EXISTS `cate_news`;
CREATE TABLE IF NOT EXISTS `cate_news` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `status` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(4) NOT NULL DEFAULT '0',
  `display` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_models__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cate_news`
--

INSERT INTO `cate_news` (`id`, `name`, `slug`, `_lft`, `_rgt`, `parent_id`, `status`, `is_home`, `display`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'root', NULL, 1, 6, NULL, 'active', 0, NULL, '2019-07-11 09:54:29', 'admin', '2019-07-11 09:54:29', 'admin'),
(2, 'Xu hướng thời trang', NULL, 4, 5, 1, 'active', 0, NULL, '2020-07-16 00:00:00', 'admin', NULL, NULL),
(3, 'Khuyến mãi', NULL, 2, 3, 1, 'active', 0, NULL, '2020-07-16 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cate_product`
--

DROP TABLE IF EXISTS `cate_product`;
CREATE TABLE IF NOT EXISTS `cate_product` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `status` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(4) NOT NULL DEFAULT '0',
  `display` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_models__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cate_product`
--

INSERT INTO `cate_product` (`id`, `name`, `slug`, `_lft`, `_rgt`, `parent_id`, `status`, `is_home`, `display`, `meta_description`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'root', NULL, 1, 6, NULL, 'active', 0, NULL, '', '2019-07-11 09:54:29', 'admin', '2019-07-11 09:54:29', 'admin'),
(2, 'Thời trang Nam', NULL, 4, 5, 1, 'active', 0, NULL, NULL, '2020-07-17 00:00:00', 'admin', NULL, NULL),
(3, 'Thời trang Nữ', NULL, 2, 3, 1, 'active', 0, NULL, NULL, '2020-07-17 00:00:00', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_name` varchar(50) NOT NULL DEFAULT '0',
  `image` varchar(50) NOT NULL DEFAULT '0',
  `comment` varchar(3000) NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `rate` int(11) NOT NULL DEFAULT '0',
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT '0',
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `guest_name`, `image`, `comment`, `status`, `rate`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(2, 'Thúy Trâm', '/uploads/comments/578vUR4bZc.jpeg', '<p>Nh&igrave;n chung m&igrave;nh rất h&agrave;i l&ograve;ng về cả gi&aacute; cả lẫn chất lượng sản phẩm. N&ecirc;n m&igrave;nh đ&aacute;nh gi&aacute; 5 sao. Tuy nhi&ecirc;n, mong l&agrave; cửa h&agrave;ng n&ecirc;n l&agrave;m việc lại với b&ecirc;n đối t&aacute;c giao h&agrave;ng. Lần n&agrave;y l&agrave; giao h&agrave;ng nhanh chịu tr&aacute;ch nhiệm giao h&agrave;ng với đơn của m&igrave;nh. Th&aacute;i độ phục vụ thiếu chuy&ecirc;n nghiệp. Mong b&ecirc;n bạn xem x&eacute;t vấn đề n&agrave;y.</p>', 'active', 5, 'admin', '2019-06-12 17:06:20', 'admin', '2020-06-24 00:00:00'),
(3, 'Bảo Trâm', '/uploads/comments/7upRlMKFFx.jpeg', '<p>H&agrave;ng đ&uacute;ng chuẩn model VN/A, H&agrave;ng nguy&ecirc;n seal, chưa k&iacute;ch hoạt. Giao h&agrave;ng 2h ổn. chưa tới 2 tiếng đ&atilde; tới nơi. X&agrave;i được 2 ng&agrave;y chưa thấy vấn đề g&igrave;.</p>', 'active', 5, 'admin', '2019-06-12 17:06:03', 'admin', '2020-06-24 00:00:00'),
(11, 'Võ Yến Vy', '/uploads/comments/xs0gDpMmIo.jpeg', '<p>M&aacute;y mới mua được 1 th&aacute;ng rất &iacute;t khi d&ugrave;ng đến m&agrave; một ng&agrave;y đẹp trời đen x&igrave; m&agrave;n h&igrave;nh, sạc kh&ocirc;ng v&agrave;o. Gọi l&ecirc;n tiki khiếu nại th&igrave; được hướng dẫn mang ra địa chỉ bảo h&agrave;nh của h&atilde;ng apple để bảo h&agrave;nh. Mang ra bảo h&agrave;nh th&igrave; được b&aacute;o lỗi phần cứng đ&atilde; gửi Miền Nam xử l&yacute;.</p>', 'active', 1, 'admin', '2019-07-18 10:07:14', 'admin', '2020-06-24 00:00:00'),
(12, 'Kim Dung', '/uploads/comments/Sgc0XZd61D.jpeg', '<p>Nội dung đ&aacute;nh gi&aacute; sản phẩm</p>', 'active', 4, 'admin', '2020-06-12 00:00:00', 'admin', '2020-06-24 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `list_recall`
--

DROP TABLE IF EXISTS `list_recall`;
CREATE TABLE IF NOT EXISTS `list_recall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(11) NOT NULL DEFAULT '0',
  `status` varchar(50) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) NOT NULL DEFAULT '0',
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `list_recall`
--

INSERT INTO `list_recall` (`id`, `phone`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(29, '0963481000', 'inactive', '2019-06-29 22:35:54', 'admin', '2019-06-29 22:35:54', 'admin'),
(31, '0963481001', 'active', '2019-07-01 21:56:59', 'admin', '2019-07-01 21:56:59', 'admin'),
(32, '0999999999', '0', '2020-07-08 00:00:00', 'customer', NULL, '0'),
(33, '0123456789', 'inactive', '2020-07-08 00:00:00', 'customer', NULL, '0'),
(34, '0989870284', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(35, '01262332448', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(36, '0989870284', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(37, '0989870284', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(38, '23313212132', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(39, '23313212132', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(40, '23313212132', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(41, '23313212132', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(42, '23313212132', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(43, '23313212132', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(44, '23313212132', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(45, '0989870284', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(46, '0989870284', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(47, '0989870284', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(48, '0989870284', 'inactive', '2020-07-11 00:00:00', 'customer', NULL, '0'),
(49, '0989870284', 'inactive', '2020-07-14 00:00:00', 'customer', NULL, '0'),
(50, '0989870284', 'inactive', '2020-07-14 00:00:00', 'customer', NULL, '0'),
(51, '0989870284', 'inactive', '2020-07-14 00:00:00', 'customer', NULL, '0'),
(52, '0989870284', 'inactive', '2020-07-14 00:00:00', 'customer', NULL, '0'),
(53, '0989870284', 'inactive', '2020-07-14 00:00:00', 'customer', NULL, '0'),
(54, '0762332448', 'inactive', '2020-07-14 00:00:00', 'customer', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `status` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_home` tinyint(4) NOT NULL DEFAULT '0',
  `display` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '_self',
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `menu_models__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `link`, `slug`, `_lft`, `_rgt`, `parent_id`, `status`, `is_home`, `display`, `target`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'root', NULL, NULL, 1, 18, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Trang chủ', '{\"type\":\"link\",\"value\":\"http:\\/\\/lutaden.xyz\\/\"}', NULL, 16, 17, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(3, 'Giới thiệu', '{\"type\":\"category\",\"value\":\"9\"}', NULL, 14, 15, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(4, 'Dịch vụ', '{\"type\":\"category\",\"value\":\"3\"}', NULL, 12, 13, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(5, 'Bảng giá', '{\"type\":\"news\",\"value\":\"2\"}', NULL, 10, 11, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', '2020-07-06 00:00:00', 'admin'),
(6, 'Đặt lịch hẹn', '{\"type\":\"link\",\"value\":\"dat-lich-hen.html\"}', NULL, 8, 9, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(7, 'Ưu đãi', '{\"type\":\"category\",\"value\":\"1\"}', NULL, 6, 7, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', NULL, NULL),
(8, 'Liên hệ', '{\"type\":\"link\",\"value\":\"\\/he-thong-chi-nhanh.html\"}', NULL, 4, 5, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', '2020-07-07 00:00:00', 'admin'),
(9, 'Câu hỏi thường gặp', '{\"type\":\"link\",\"value\":\"\\/cau-hoi-thuong-gap.html\"}', NULL, 2, 3, 1, 'active', 0, NULL, '_self', '2020-07-05 00:00:00', 'admin', '2020-07-07 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_01_07_073615_create_tagged_table', 1),
(2, '2014_01_07_073615_create_tags_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` smallint(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `cat_id` int(10) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `image_extra` varchar(2000) DEFAULT NULL,
  `short` text,
  `description` text,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `price_sale` varchar(50) DEFAULT NULL,
  `sale_start` date DEFAULT NULL,
  `sale_end` date DEFAULT NULL,
  `friendly_title` varchar(250) DEFAULT NULL,
  `friendly_url` varchar(250) DEFAULT NULL,
  `metakey` varchar(250) DEFAULT NULL,
  `metadesc` text,
  `config` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `title`, `slug`, `status`, `cat_id`, `picture`, `image_extra`, `short`, `description`, `created`, `created_by`, `modified`, `modified_by`, `price`, `price_sale`, `sale_start`, `sale_end`, `friendly_title`, `friendly_url`, `metakey`, `metadesc`, `config`) VALUES
(1, 'Iphone 11', 'iphone-11', 'active', 12, '{\"src\":\"\\/upload\\/1\\/product\\/iphone-11.jpg\",\"alt\":\"\\u0110\\u00e2y l\\u00e0 alt \\u1ea3nh ch\\u00ednh.\"}', '[{\"src\":\"\\/upload\\/1\\/product\\/iphone-11-1.jpg\",\"alt\":\"\\u0110\\u00e2y l\\u00e0 alt \\u1ea3nh ph\\u1ee5 1\"},{\"src\":\"\\/upload\\/1\\/product\\/iphone-11-2.jpg\",\"alt\":null},{\"src\":\"\\/upload\\/1\\/product\\/iphone-11-3.jpg\",\"alt\":null},{\"src\":\"\\/upload\\/1\\/product\\/iphone-11-4.jpg\",\"alt\":\"\\u0110\\u00e2y l\\u00e0 alt \\u1ea3nh ph\\u1ee5 4\"}]', 'Đây là Iphone 11', '<p><strong><em>Đ&acirc;y l&agrave; Iphone 11</em></strong></p>\r\n\r\n<p><img alt=\"\" src=\"http://proj_cms.test/upload/1/product/iphone-11-1.jpg\" style=\"height:216px; width:384px\" /></p>', '2019-09-10 15:09:19', 'admin', '2019-09-15 03:09:34', 'admin', '1000000', '800000', '2019-08-01', '2019-08-25', NULL, NULL, NULL, NULL, '{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\",\"5\":\"1\"}'),
(2, 'Samsung Galaxy Note 10', 'samsung-galaxy-note-10', 'active', 2, '/uploads/article/test.jpg', '[{\"src\":\"\\/upload\\/1\\/product\\/ss12.jpg\",\"alt\":null},{\"src\":\"\\/upload\\/1\\/product\\/ss14.jpg\",\"alt\":null},{\"src\":\"\\/upload\\/1\\/product\\/ss15.jpg\",\"alt\":null}]', 'Đây là mô tả sản phẩm', '<p><strong>Đ&acirc;y l&agrave; m&ocirc; tả sản phẩm</strong></p>\r\n<p><strong><img style=\"height: 600px; width: 600px;\" src=\"http://proj_cms.test/upload/1/product/ss15.jpg\" alt=\"\" /></strong></p>', '2019-09-12 10:09:32', 'admin', '2020-07-18 15:42:00', 'admin', '2000000', '1950000', '2019-09-21', '2019-09-30', NULL, NULL, NULL, NULL, '{\"1\":\"1\",\"2\":\"1\",\"3\":\"1\",\"5\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

DROP TABLE IF EXISTS `product_size`;
CREATE TABLE IF NOT EXISTS `product_size` (
  `size_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `status` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`size_id`) USING BTREE,
  KEY `menu_models__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_value` varchar(50) NOT NULL DEFAULT '0',
  `value` longtext,
  `status` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key_value`, `value`, `status`, `created`, `created_by`, `modified`, `modified_by`) VALUES
(1, 'setting-main', '{\"indextitle\":\"Nha Khoa Lutadent\",\"metakey\":\"123\",\"metadesc\":\"456\",\"meta_head\":\"423\",\"extra_foot\":\"456\",\"logo\":\"\\/uploads\\/weblink\\/6FWcYeuuaw.png\",\"hotline\":\"0963481205\",\"email\":\"lehung.3011@gmail.com\",\"address\":\"55 \\u0110\\u01b0\\u1eddng 1, P.TNPB, Qu\\u1eadn 9\"}', NULL, '2019-06-28 10:06:46', 'admin', '2020-06-25 00:00:00', 'admin'),
(4, 'setting-email', '{\"smtp_username\":\"hieufoodsshop@gmail.com\",\"smtp_password\":null,\"smtp_name\":\"Hi\\u1ebfu Fashion\",\"bcc_guest_contact\":\"lehung.3011@gmail.com,vanhung.it.3011@gmail.com,htkdung.2311@gmail.com\",\"bcc_guest_recall\":\"lehung.3011@gmail.com\",\"bcc_guest_order\":null}', NULL, '2019-06-18 06:06:22', 'admin', '2020-07-16 00:00:00', 'admin'),
(6, 'setting-social', '{\"facebook\":{\"url\":\"https:\\/\\/www.facebook.com\\/nhakhoaLutadent\\/\",\"icon\":\"fa-facebook\"},\"google\":{\"url\":\"https:\\/\\/www.youtube.com\\/channel\\/UCoMC4vMARsZ5oAE6aO_mxEQ\",\"icon\":\"fa-youtube\"},\"twitter\":{\"url\":\"https:\\/\\/twitter.com\\/lehung3011\",\"icon\":\"fa-twitter\"}}', NULL, '2019-06-28 10:06:09', 'admin', '2020-06-25 00:00:00', 'admin'),
(7, 'setting-script', '{\"script_head\":\"<script>\\r\\n    window.dataLayer = window.dataLayer || [];\\r\\n    function gtag(){dataLayer.push(arguments);}\\r\\n    gtag(\'js\', new Date());\\r\\n\\r\\n    gtag(\'config\', \'UA-141102928-1\');\\r\\n<\\/script>\",\"google_map\":\"<iframe src=\\\"https:\\/\\/www.google.com\\/maps\\/embed?pb=!1m18!1m12!1m3!1d3918.2851212441365!2d106.80074885111087!3d10.865905092222869!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752745aaaaaaab%3A0x1445c7dda5808d58!2zS2h1IER1IGzhu4tjaCBWxINuIGjDs2EgU3Xhu5FpIFRpw6pu!5e0!3m2!1svi!2s!4v1568558704749!5m2!1svi!2s\\\" width=\\\"600\\\" height=\\\"450\\\" frameborder=\\\"0\\\" style=\\\"border:0;\\\" allowfullscreen=\\\"\\\"><\\/iframe>\",\"google_analyst\":\"<script>\\r\\n    window.dataLayer = window.dataLayer || [];\\r\\n    function gtag(){dataLayer.push(arguments);}\\r\\n    gtag(\'js\', new Date());\\r\\n\\r\\n    gtag(\'config\', \'UA-141102928-1\');\\r\\n<\\/script>\"}', NULL, NULL, 'admin', '2019-09-15 14:09:24', 'admin'),
(8, 'setting-chat', '{\"facebook\":\"{\\\"page_id\\\":\\\"123456\\\",\\\"position\\\":\\\"right\\\",\\\"status\\\":\\\"inactive\\\"}\",\"zalo\":\"{\\\"page_id\\\":\\\"123456\\\",\\\"position\\\":\\\"left\\\",\\\"status\\\":\\\"active\\\"}\",\"service\":\"{\\\"page_id\\\":\\\"<script>\\\\r\\\\n    window.dataLayer = window.dataLayer || [];\\\\r\\\\n    function gtag(){dataLayer.push(arguments);}\\\\r\\\\n    gtag(\'js\', new Date());\\\\r\\\\n\\\\r\\\\n    gtag(\'config\', \'UA-141102928-1\');\\\\r\\\\n<\\\\\\/script>\\\",\\\"position\\\":\\\"right\\\",\\\"status\\\":\\\"inactive\\\"}\"}', NULL, NULL, 'admin', '2019-09-16 05:09:03', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `link` varchar(200) NOT NULL,
  `thumb` text,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `status` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `name`, `description`, `link`, `thumb`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'RĂNG SÁNG DÁNG XINH', 'Trọn bộ 8 răng sứ chỉ 12 triệu. Trọn bộ 16 răng chỉ từ 22 triệu', 'http://nhakhoalutadent.com/bai-viet/nieng-rang-tham-my-27.html', '/uploads/slider/UoqTqeewNG.jpeg', '2019-04-15 00:00:00', 'admin', '2020-06-25 00:00:00', 'hailan', 'active'),
(2, 'NIỀNG RĂNG TRONG SUỐT', 'Ưu đãi lên đến 50%', 'https://nhakhoalutadent.com/bai-viet/giam-50-nieng-rang-trong-suot-47.html', '/uploads/slider/BUZ8o7Nzjw.jpeg', '2019-04-18 00:00:00', 'admin', '2020-06-25 00:00:00', 'hailan', 'active'),
(3, 'Trả góp 0% lãi suất', 'Niềng răng trả góp - Răng sứ trả góp 0% lãi suất chỉ từ 1 triệu/tháng', 'https://nhakhoalutadent.com/bai-viet/lam-rang-tra-gop-chi-tu-699kthang-45.html', '/uploads/slider/nVgqrnMTDG.jpeg', '2019-04-24 00:00:00', 'admin', '2020-06-25 00:00:00', 'hailan', 'active'),
(4, 'ewrewdasda', 'rewrdsahodays dasdoiusaoidas', 'http://dsdsad', '/uploads/slider/nVgqrnMTDG.jpeg', '2020-06-25 00:00:00', 'hailan', NULL, NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tagging_tagged`
--

DROP TABLE IF EXISTS `tagging_tagged`;
CREATE TABLE IF NOT EXISTS `tagging_tagged` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_slug` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tagging_tagged_taggable_id_index` (`taggable_id`),
  KEY `tagging_tagged_taggable_type_index` (`taggable_type`),
  KEY `tagging_tagged_tag_slug_index` (`tag_slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tagging_tags`
--

DROP TABLE IF EXISTS `tagging_tags`;
CREATE TABLE IF NOT EXISTS `tagging_tags` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suggest` tinyint(1) NOT NULL DEFAULT '0',
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `tag_group_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tagging_tags_slug_index` (`slug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `level` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `created_by` varchar(45) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` varchar(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `fullname`, `password`, `avatar`, `level`, `created`, `created_by`, `modified`, `modified_by`, `status`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'ltH9r6RUdr.jpeg', 'admin', '2014-12-10 08:55:35', 'admin', '2020-06-12 00:00:00', 'hailan', 'active'),
(2, 'lehung3011', 'lehung.3011@gmail.com', 'Lê Hùng', '6d08f251456f1b9b2538b0fb66223142', 'nuaKQiRFvf.jpeg', 'member', '2020-06-12 00:00:00', 'hailan', NULL, NULL, 'active');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
