$(document).ready(function () {
    let $btnSearch = $("button#btn-search");
    let $btnClearSearch = $("button#btn-clear-search");

    let $inputSearchField = $("input[name  = search_field]");
    let $inputSearchValue = $("input[name  = search_value]");
    let $selectChangeAttr = $("select[name = select_change_attr]");
    let $selectChangeAttrAjax = $("select[name =  select_change_attr_ajax]");

    /*===================================================================*/
    //edit slug post
    POST.init();
    
    /*===================================================================*/
    $("a.select-field").click(function (e) {
        e.preventDefault();

        let field = $(this).data("field");
        let fieldName = $(this).html();
        $("button.btn-active-field").html(
            fieldName + ' <span class="caret"></span>'
        );
        $inputSearchField.val(field);
    });

    /*===================================================================*/
    $btnSearch.click(function () {
        var pathname = window.location.pathname;
        let params = ["filter_status"];
        let searchParams = new URLSearchParams(window.location.search); // ?filter_status=active

        let link = "";
        $.each(params, function (key, param) {
            // filter_status
            if (searchParams.has(param)) {
                link += param + "=" + searchParams.get(param) + "&"; // filter_status=active
            }
        });

        let search_field = $inputSearchField.val();
        let search_value = $inputSearchValue.val();

        if (search_value.replace(/\s/g, "") == "") {
            alert("Nhập vào giá trị cần tìm !!");
        } else {
            window.location.href =
                pathname +
                "?" +
                link +
                "search_field=" +
                search_field +
                "&search_value=" +
                search_value;
        }
    });

    /*===================================================================*/
    $btnClearSearch.click(function () {
        var pathname = window.location.pathname;
        let searchParams = new URLSearchParams(window.location.search);

        params = ["filter_status"];

        let link = "";
        $.each(params, function (key, param) {
            if (searchParams.has(param)) {
                link += param + "=" + searchParams.get(param) + "&";
            }
        });

        window.location.href = pathname + "?" + link.slice(0, -1);
    });

    /*===================================================================*/
    $(".btn-delete").on("click", function () {
        if (!confirm("Bạn có chắc muốn xóa phần tử?")) return false;
    });

    /*===================================================================*/
    $selectChangeAttr.on("change", function () {
        let selectValue = $(this).val();
        let url = $(this).data("url");
        console.log(url.replace("value_new", selectValue));
        window.location.href = url.replace("value_new", selectValue);
    });

    /*===================================================================*/
    $selectChangeAttrAjax.on("change", function () {
        let select_value = $(this).val();
        let $url = $(this).data("url");
        let csrf_token = $("input[name=csrf-token]").val();

        $.ajax({
            url: $url.replace("value_new", select_value),
            type: "GET",
            dataType: "json",
            headers: {
                "X-CSRF-TOKEN": csrf_token,
            },
            success: function (result) {
                if (result) {
                    $.notify("Cập nhật giá trị thành công!", {
                        position: "top center",
                        className: "success",
                    });
                } else {
                    console.log(result);
                }
            },
        });
    });

    /*===================================================================*/
    var editor_config = {
        path_absolute: "/",
        selector: "textarea.tinyeditor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern",
        ],
        toolbar:
            "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback: function (field_name, url, type, win) {
            var x =
                window.innerWidth ||
                document.documentElement.clientWidth ||
                document.getElementsByTagName("body")[0].clientWidth;
            var y =
                window.innerHeight ||
                document.documentElement.clientHeight ||
                document.getElementsByTagName("body")[0].clientHeight;

            var cmsURL =
                editor_config.path_absolute +
                "laravel-filemanager?field_name=" +
                field_name;
            if (type == "image") {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file: cmsURL,
                title: "Filemanager",
                width: x * 0.8,
                height: y * 0.8,
                resizable: "yes",
                close_previous: "no",
            });
        },
    };

    tinymce.init(editor_config);

    /*===================================================================*/
    var route_prefix = "/laravel-filemanager";
    if ($("body").find("#lfm").length > 0) {
        $("#lfm").filemanager("image", { prefix: route_prefix });
    }
    /*===================================================================*/
    // Upload image button
    var lfm_button = function(id, type, options) 
    {
        let button = document.getElementById(id);
        if(button)
        {
            button.addEventListener('click', function () {
                var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
                var target_input = document.getElementById(button.getAttribute('data-input'));
                var target_preview = document.getElementById(button.getAttribute('data-preview'));
    
                window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
                window.SetUrl = function (items) 
                {
                    var file_path = items.map(function (item) {
                        return item.url;
                    }).join(',');
    
                    // set the value of the desired input to image url
                    target_input.value = file_path;
                    target_input.dispatchEvent(new Event('change'));
    
                    // clear previous preview
                    target_preview.innerHtml = '';
    
                    // set or change the preview image src
                    items.forEach(function (item) {
                        let img = document.createElement('img')
                        img.setAttribute('style', 'width: 100%;')
                        img.setAttribute('src', item.thumb_url)
                        img.setAttribute('class', 'picture_thumbnail')
                        target_preview.appendChild(img);
                    });
                    button.classList.add("hide");
    
                    //Remove image
                    var button_del = document.createElement("button");
                    button_del.innerHTML = "<i class='fa fa-trash-o'></i> Xóa ảnh";
                    button_del.setAttribute('class', 'btn btn-danger btn-delete-lfm');
                    button_del.setAttribute('id', 'btn-delete-lfm');
                    button_del.setAttribute('style', 'margin-top: 10px;');
                    button_del.setAttribute('type', 'button');
                    target_preview.appendChild(button_del);
                    button_del.addEventListener('click', function () {
                        button.classList.remove("hide");
                        target_preview.innerHTML = '';
                        target_input.value = '';
                    });
                    // trigger change event
                    target_preview.dispatchEvent(new Event('change'));
                };
            });
        }
        
        if($('body').find('#btn-delete-lfm').length)
        {
            let button_delete = document.getElementById('btn-delete-lfm');
            button_delete.addEventListener('click', function () {
                var target_preview = document.getElementById(button.getAttribute('data-preview'));
                var target_input = document.getElementById(button.getAttribute('data-input'));
                button.classList.remove("hide");
                target_preview.innerHTML = '';
                target_input.value = '';
            });
        }
    };
    lfm_button('btn-upload-lfm', 'image', {prefix: route_prefix});
    

    /*===================================================================*/
    if($('body').find('.tags').length)
    {
        $(".tags").selectize({
            delimiter: ",",
            persist: false,
            valueField: "tag",
            labelField: "tag",
            searchField: "tag",
            options: tags,
            create: function (input) {
                return {
                    tag: input,
                };
            },
        });
    }
    
});

var ROOT = document.location.origin +  '/';
console.log(ROOT);
var POST = {
	permalink: function(str_slug){
		var EXT = '.html';
		str_slug = POST.slugStr(str_slug);
		var url = ROOT + str_slug + EXT;
		return url;
	},
	slugStr: function(str) {
		var slug;
		slug = str.toLowerCase();
		slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
		slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
		slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
		slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
		slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
		slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
		slug = slug.replace(/đ/gi, 'd');
		slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\“|\”|\:|\;|_/gi, '');
		slug = slug.replace(/ /gi, "-");
		slug = slug.replace(/\-\-\-\-\-/gi, '-');
		slug = slug.replace(/\-\-\-\-/gi, '-');
		slug = slug.replace(/\-\-\-/gi, '-');
		slug = slug.replace(/\-\-/gi, '-');
		slug = '@' + slug + '@';
		slug = slug.replace(/\@\-|\-\@|\@/gi, '');
		return slug;
	},
	slugEdit: function(){
		$(document).on('click', '.edit-slug', function(e){
			$(this).text('OK');
			$(this).attr('class', 'save btn btn-success btn-sm');
			var box = $('#edit-slug-box');
			var input = box.find('input');
			input.focus();
			var val = input.val();
			box.attr('data-slug', val);

			//edit-slug-buttons
			box.find('#edit-slug-buttons').append('<button/>');
			box.find('button:last').text('Hủy');
			box.find('button:last').attr('type', 'button');
			box.find('button:last').attr('class', 'cancel btn btn-danger btn-sm');

			//editable-post-name
			box.find('#editable-post-name').html(input);

			//sample-permalink
			box.find('#sample-permalink').html($('#editable-post-name'));
			box.find('#sample-permalink').prepend(ROOT);

			box.find('input').attr('type', 'text');
			box.find('input').focus();
		});
	},
	slugCancel: function(){
		$(document).on('click', '.cancel', function(e){
			$(this).remove();
			var box = $('#edit-slug-box');
			var input = box.find('input');
			var slug = box.data('slug');
			var href = POST.permalink(slug);
			box.removeAttr('data-slug');
			box.find('.save').text('Chỉnh sửa');
			box.find('.save').attr('class', 'edit-slug btn btn-primary btn-sm');

			//sample-permalink
			box.find('#sample-permalink').wrapInner('<a/>');
			box.find('a').attr('href', href);
			box.find('a').attr('target', '_blank');
			box.find('#sample-permalink').append(input);
			box.find('input').val(slug);
			box.find('input').attr('type', 'hidden');

			//editable-post-name
			box.find('#editable-post-name').text(slug);
		});
	},
	slugSave: function(){
		$(document).on('click', '.save', function(e){
			var box = $('#edit-slug-box');
			var input = box.find('input');
			var new_slug = input.val();
			var old_slug = box.data('slug');
			// var slug = new_slug ? new_slug : old_slug;

			var new_slug_by_title = $('#post-title').find('input[name=title]').val();
			var slug = new_slug ? new_slug : new_slug_by_title;

			slug = POST.slugStr(slug);
			var href = POST.permalink(slug);
			//sample-permalink
			box.find('#sample-permalink').wrapInner('<a/>');
			box.find('a').attr('href', href);
			box.find('a').attr('target', '_blank');
			box.find('#sample-permalink').append(input);
			box.find('input').val(slug);
			box.find('input').attr('type', 'hidden');
			box.find('.save').text('Chỉnh sửa');
			box.find('.save').attr('class', 'edit-slug btn btn-primary btn-sm');
			//editable-post-name
			box.find('#editable-post-name').text(slug);
			box.find('.cancel').remove();
			box.removeAttr('data-slug');
			$(this).text('Chỉnh sửa');
			$(this).attr('class', 'edit-slug btn btn-primary btn-sm');
		});
		//check keyup keypress ENTER
		$(document).on('keyup keypress', '.post-slug', function(e) {
			var keyCode = e.keyCode || e.which;
			var box = $('#edit-slug-box');
			switch(keyCode)
			{
				case 13:
					box.find('.save').click();
					e.preventDefault();
					return false;
				break;
				case 27:
					box.find('.cancel').click();
					e.preventDefault();
					return false;
				break;
			}
		});
	},
	slugAdd: function(){
        // console.log(sub);
		if((typeof sub !== 'undefined') && sub=='add')
		{
			$(document).on('keyup', '#title', function(){
				var val = $(this).val();
				var slug = POST.slugStr(val);
				var href = POST.permalink(slug);
                var box = $('#edit-slug-box');
				if(val) {
					box.removeAttr('style');
				} else {
					box.css('z-index', -1);
				}
				if(box.find('.save').length>0) {
					box.find('.save').click();
				}
				box.find('input').val(slug);
				box.find('a').attr('href', href);
				box.find('#editable-post-name').text(slug);
			});
		}
	},
	init: function(){
		POST.slugAdd();
		POST.slugEdit();
		POST.slugSave();
		POST.slugCancel();
	}
}
