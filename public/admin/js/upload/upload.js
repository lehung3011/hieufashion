var ROOT = document.location.origin +  '/';
$(function () {
    UPLOAD.init();
});
var UPLOAD = {
    box: function (name) {
        return $("#gallery-box-" + name);
    },
    popup: function (name) {
        var box = UPLOAD.box(name);
        var btn = box.find(".btn-popup");
        btn.fancybox({
            width: "90%",
            height: "90%",
            padding: 0,
            type: "iframe",
            autoScale: false,
            fitToView: false,
            autoSize: false,
            autoDimensions: false,
        });
    },
    change: function (name) {
        var arr = [];
        var input = $("#" + name);
        var str = input.val();
        if (str.substr(0, 1) === "[") {
            var arr = JSON.parse(str);
        } else {
            arr.push(str);
        }
        UPLOAD.html(name, arr);
        UPLOAD.sort(name);
        UPLOAD.setVal(name);
    },
    html: function (name, arr) {
        var box = UPLOAD.box(name);
        if (!box.find(".galleries").length) {
            box.prepend('<div class="galleries">');
        }
        var items = box.find(".galleries");
        var count = items.find(".gallery-item").length;
        $.each(arr, function (i, v) {
            i += count;
            var val = v.replace(UPLOAD_URL, "");
            var cls = "gallery-item gallery-item-" + i;
            items.append('<div class="' + cls + '">');
            var item = items.find(".gallery-item-" + i);
            var title = v.replace(/^.*?([^\/]+)\..+?$/, "$1");
            item.append('<div class="inner">');
            item.find(".inner").append("<img>");
            item.find("img").attr("src", v);
            item.find(".inner").append("<button>");
            item.find("button").addClass("del button");
            item.find("button").attr(
                "onclick",
                "UPLOAD.del('" + name + "'," + i + ");"
            );
            item.find(".inner").append("<input>");
            item.find(".inner").append("<input>");
            item.find("input:first").val(title);
            item.find("input:first").attr("name", name + "[" + i + "][title]");
            item.find("input:last").val(val);
            item.find("input:first").attr("name", name + "[" + i + "][value]");
            item.find("input").attr("type", "hidden");
        });
    },
    del: function (name, id) {
        var box = UPLOAD.box(name);
        if (id != "all") {
            box.find(".gallery-item-" + id).remove();
        } else {
            box.find(".galleries").remove();
        }
        UPLOAD.setVal(name);
    },
    sort: function (name) {
        var box = UPLOAD.box(name);
        box.find(".galleries").sortable({
            start: function (e, ui) {
                var h = ui.item.height();
                ui.placeholder.height(h);
            },
            update: function (event, ui) {
                UPLOAD.setVal(name);
            },
        });
    },
    setVal: function (name) {
        var box = UPLOAD.box(name);
        var items = box.find(".gallery-item");
        var input = box.find("#" + name);
        if (items.length) {
            var arr = [];
            items.each(function (i, e) {
                var onclick = "UPLOAD.del('" + name + "'," + i + ");";
                $(e)
                    .find("img")
                    .attr(
                        "onclick",
                        "UPLOAD.editName('" + name + "'," + i + ");"
                    );
                $(e).find(".button").attr("onclick", onclick);
                $(e).attr("class", "gallery-item gallery-item-" + i);
                $(e)
                    .find("input:first")
                    .attr("name", "gallery[" + i + "][title]");
                $(e)
                    .find("input:last")
                    .attr("name", "gallery[" + i + "][value]");
                var src = $(e).find("img").attr("src");
                var picture = src.replace(UPLOAD_URL, "");
                arr.push(picture);
            });
            input.val(arr.join());
        } else {
            input.val("");
            box.find(".galleries").remove();
        }
        UPLOAD.setBtn(name);
    },
    setBtn: function (name) {
        var box = UPLOAD.box(name);
        var btn = box.find(".btn-submit");
        if (box.find(".gallery-item").length) {
            var btnDelAll;
            if (box.find(".btn-del-all").length == 0) {
                btnDelAll = btn.clone().appendTo(box.find(".upload-btn"));
                btnDelAll.html("Xoá tất cả ảnh");
                btnDelAll.attr("href", "javascript:void(0);");
                btnDelAll.attr("class", "button btn-del-all");
                btnDelAll.attr("onclick", "UPLOAD.del('" + name + "','all');");
            }
        } else {
            box.find(".btn-del-all").remove();
        }
    },
    setName: function (f, name, id) {
        var box = UPLOAD.box(name);
        var item = box.find(".gallery-item-" + id);
        item.find("input:first").val(f.value);
        parent.$.fancybox.close();
    },
    editName: function (name, id) {
        var box = UPLOAD.box(name);
        var item = box.find(".gallery-item-" + id).clone();
        item.removeAttr("onclick");
        item.find(".del").remove();
        item.find("input.edit-name").remove();
        item.removeClass("ui-sortable-handle");
        item.append('<input class="edit-name">');
        var title = item.find("input:first").val();
        var input = item.find(".edit-name");
        input.addClass("form-control");
        input.attr("type", "text");
        input.attr(
            "onchange",
            "UPLOAD.setName(this,'" + name + "'," + id + ")"
        );
        input.val(title);
        $.fancybox.open({
            width: 400,
            height: 400,
            autoSize: 0,
            autoScale: 0,
            content: item,
            transitionIn: "elastic",
        });
    },
    dropzone: function (product_id) {
        var myDropzoneOptions = { 
            // addRemoveLinks: true,
            url: $("#mydropzone").attr('data-url'),
            autoProcessQueue: true,
            acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
            maxFilesize: 5, // MB
            parallelUploads: 2,
            thumbnailHeight: 120,
            thumbnailWidth: 120,
            autoDiscover: false,
            resizeQuality: 0.6,
            resizeWidth: 1000,
            maxFiles: 3,
            clickable: true,
            renameFile: function (file) {
                let mine = file.type;
                var extension;
                switch (mine) {
                    case "image/png":
                        extension = ".png";
                        break;
                    case "image/jpeg":
                        extension = ".jpg";
                        break;
                    case "image/jpg":
                        extension = ".jpg";
                        break;
                    case "image/gif":
                        extension = ".gif";
                        break;
                    case "image/bmp":
                        extension = ".bmp";
                        break;
                }
                let newName = new Date().getTime() + extension;
                return newName;
            },
            dictDefaultMessage:
                "Click chọn hoặc kéo thả hình ảnh vào đây để Upload.",
            previewTemplate:
                '<div class="dz-preview dz-complete dz-image-preview"> ' +
                '<div class="dz-image"><img data-dz-thumbnail > </div>' +
                '<div class="dz-details"> ' +
                '<div class="dz-size" data-dz-size></div> ' +
                '<div class="dz-filename"><span data-dz-name></span></div>' +
                "</div>" +
                '<div class="dz-progress">' +
                '<span class="dz-upload" data-dz-uploadprogress></span> ' +
                "</div>" +
                '<div class="dz-error-message">' +
                "<span data-dz-errormessage></span> " +
                "</div>" +
                '<div class="dz-success-mark">' +
                '<svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns"><title>Check</title><defs></defs><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage" > <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup" ></path> </g> </svg>' +
                "</div>" +
                '<div class="dz-error-mark"> ' +
                '<svg  width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" > <title>Error</title> <defs></defs> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage" > <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475" > <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup" ></path></g> </g> </svg>' +
                "</div>" +
                '<a class="dz-remove" href="javascript:;" data-dz-remove data-pid="' +
                product_id +
                '"><i class="fa fa-times"></i></a>' +
                "</div>",
            init: function () {
                myDropzone = this;
                var list_image = [];
                if (typeof Storage !== "undefined") {
                    if (localStorage.getItem("listImagesUpload") !== null) {
                        list_image = JSON.parse(
                            localStorage.getItem("listImagesUpload")
                        );
                    }
                    if (list_image.length !== 0) {
                        $.get(
                            root +
                                "ajax?do=upload&id=" +
                                product_id +
                                "&task=list&data=" +
                                encodeURIComponent(list_image),
                            function (data) {
                                $.each(JSON.parse(data), function (key, value) {
                                    var mockFile = {
                                        name: value.name,
                                        size: value.size,
                                        upload: { filename: value.name },
                                    };
                                    var urlUpload =
                                        document.location.hostname ==
                                        "localhost"
                                            ? document.location.origin +
                                              "/dien_may_gia_goc"
                                            : document.location.origin;
                                    myDropzone.options.addedfile.call(
                                        myDropzone,
                                        mockFile
                                    );
                                    myDropzone.files.push(mockFile);
                                    myDropzone.options.thumbnail.call(
                                        myDropzone,
                                        mockFile,
                                        urlUpload +
                                            "/comments/product/" +
                                            product_id +
                                            "/" +
                                            value.name
                                    );
                                });
                            }
                        );

                        // Kiểm tra thêm dữ liệu vào trường ẩn
                        var box = $(".cmt-form");
                        if (box.find('input[name="cpic"]').length > 0) {
                            box.find('input[name="cpic"]').val(
                                list_image.join()
                            );
                        } else {
                            box.append(
                                '<input type="hidden" name="cpic" value="' +
                                    list_image.join() +
                                    '">'
                            );
                        }
                    }
                }
                this.on("thumbnail", function (file) {});
                this.on("addedfile", function (file) {
                    // Create the remove button
                    // var removeButton = Dropzone.createElement('<a class="dz-remove" href="javascript:;" data-dz-remove data-pid="' + product_id +'"><i class="fa fa-times"></i></a>');
                    // Capture the Dropzone instance as closure.
                    var _this = this;
                    // Listen to the click event
                    // removeButton.addEventListener('click', function (e) {

                    // });
                    // Add the button to the file preview element.
                    // file.previewElement.appendChild(removeButton);
                });
                this.on("removedfile", function (file) {
                    // Remove the file preview.
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        },
                        url:
                            root +
                            "ajax?do=upload&id=" +
                            product_id +
                            "&task=del&file=" +
                            file.upload.filename,
                        dataType: "text",
                        type: "post",
                        cache: false,
                        data: $(this).serialize(),
                        success: function (data, textStatus, jQxhr) {
                            myDropzone.removeFile(file);
                            //file.upload.filename
                            var list_image = [];
                            if (typeof Storage !== "undefined") {
                                if (
                                    localStorage.getItem("listImagesUpload") !==
                                    null
                                ) {
                                    var list_image = JSON.parse(
                                        localStorage.getItem("listImagesUpload")
                                    );
                                    list_image = jQuery.grep(
                                        list_image,
                                        function (value) {
                                            return (
                                                value != file.upload.filename
                                            );
                                        }
                                    );
                                    var myJsonString = JSON.stringify(
                                        list_image
                                    );
                                    localStorage.setItem(
                                        "listImagesUpload",
                                        myJsonString
                                    );
                                }
                            }

                            // Kiểm tra thêm dữ liệu vào trường ẩn
                            var box = $(".cmt-form");
                            if (box.find('input[name="cpic"]').length > 0) {
                                box.find('input[name="cpic"]').val(
                                    list_image.join()
                                );
                            } else {
                                box.append(
                                    '<input type="hidden" name="cpic" value="' +
                                        list_image.join() +
                                        '">'
                                );
                            }
                        },
                        error: function (jqXhr, textStatus, errorThrown) {
                            console.log(errorThrown);
                        },
                    });
                });
                this.on("success", (file) => {
                    if (file.status === "success") {
                        var img = file.upload.filename;
                        var list_image = [];
                        //  Thêm dữ liệu vào bảng lưu tạm  localStorage
                        if (typeof Storage !== "undefined") {
                            if (
                                localStorage.getItem("listImagesUpload") !==
                                null
                            ) {
                                var list_image = JSON.parse(
                                    localStorage.getItem("listImagesUpload")
                                );
                                list_image.push(img);
                                var myJsonString = JSON.stringify(list_image);
                                localStorage.setItem(
                                    "listImagesUpload",
                                    myJsonString
                                );
                            } else {
                                list_image.push(img);
                                var myJsonString = JSON.stringify(list_image);
                                localStorage.setItem(
                                    "listImagesUpload",
                                    myJsonString
                                );
                            }
                        }

                        // Kiểm tra thêm dữ liệu vào trường ẩn
                        var box = $(".cmt-form");
                        if (box.find('input[name="cpic"]').length > 0) {
                            box.find('input[name="cpic"]').val(
                                list_image.join()
                            );
                        } else {
                            box.append(
                                '<input type="hidden" name="cpic" value="' +
                                    list_image.join() +
                                    '">'
                            );
                        }
                    }
                });
            },
        };

        var elmDropzone = document.querySelector("#mydropzone");

        myDropzone = new Dropzone(elmDropzone, myDropzoneOptions);
    },
    init: function () {
        $(".gallery-box").each(function (i, e) {
            var name = $(e).attr("id");
            name = name.substr(12);
			UPLOAD.setVal(name);
			if($(this).find(".galleries").find('.gallery-item').length)
			{
				UPLOAD.sort(name);
			}

			// Khởi tạo dropzone
            var p_id = $('input[name=p_id]').val();
            Dropzone.autoDiscover = false;
            UPLOAD.dropzone(p_id);
        });
    },
};
