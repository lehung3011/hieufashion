$(function () {
    MODULE.init();
});

var MODULE = {
    elmCheck: function (elm) {
        return $("body, html").find(elm).length;
    },
    init: function () {
        // Tin Lien quan
        if (MODULE.elmCheck("#article_related")) {
            var numshow = 5;
            $("#article_related").loadAjax(
                {
                    controller: "bai-viet",
                    action: "article-related",
                    showposts: numshow,
                    data: {
                        cat_id: cat_id,
                        article_id: article_id,
                    },
                },
                function (response) {
                    setTimeout(() => {
                        $("#article_related").html(response.html);
                    }, 200);
                }
            );
        }
        // Tin Lien quan
        if (MODULE.elmCheck("#article_latest_posts")) {
            var numshow = 5;
            $("#article_latest_posts").loadAjax(
                {
                    controller: "bai-viet",
                    action: "latest-posts",
                    showposts: numshow,
                },
                function (response) {
                    setTimeout(() => {
                        $("#article_latest_posts").html(response.html);
                    }, 0);
                }
            );
        }

        // Danh sách các chuyên mục

        if (MODULE.elmCheck("#box-list-category")) {
            $("#box-list-category").loadAjax(
                {
                    controller: "chuyen-muc",
                    action: "category-list",
                },
                function (response) {
                    setTimeout(() => {
                        $("#box-list-category").html(response.html);
                    }, 0);
                }
            );
        }
    },
};
