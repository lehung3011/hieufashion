$(function () {
    var pathname = window.location.pathname;
    var link = $(".zvn-menu a").find("a").attr("href");
    $(".zvn-menu a").each(function () {
        var link = $(this).attr("href");
        if (pathname == link) {
            var elmThis = $(this);
            var elmDropmenu = elmThis.parents(".dropdown");
            if (elmDropmenu.length > 0) {
                elmDropmenu.children("a").addClass("active");
            } else {
                elmThis.addClass("active");
            }
        }
    });
    $(document).on("click", ".zvn-read-more", function () {
        var $elmContainText = $(this).siblings(".zvn-panel");
        var $elmDots = $elmContainText.find(".zvn-dots");
        var $elmMore = $elmContainText.find(".zvn-more");
        if ($elmDots.css("display") === "none") {
            $elmDots.css("display", "inline");
            $(this).text("Đọc thêm");
            $elmMore.css("display", "none");
        } else {
            $elmDots.css("display", "none");
            $(this).text("Thu gọn");
            $elmMore.css("display", "inline");
        }
    });

    $(".zvn-submit-phone").click(function () {
        var $elm_input = $("input[name='phone_customer']");
        var $elm_alert = $(".modal-body .alert");
        var $elm_this = $(this);
        var phone = $elm_input.val();
        $('<div class="loader"></div>').insertBefore($elm_input);
        $elm_alert.remove();
        $.ajax({
            url: root + "/getphone",
            method: "GET",
            data: { phone: phone },
            dataType: "json",
            error: function (a, b) {
                var arr_errors = a.responseJSON.errors.phone;
                $(".loader").remove();
                if (arr_errors.length > 0) {
                    var xhtml = `<div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>`;
                    for (var i = 0; i < arr_errors.length; i++) {
                        xhtml += `<strong>${arr_errors[i]}</strong>`;
                    }
                    xhtml += `</div>`;
                    $(xhtml).insertBefore($elm_input);
                }
            },
            success: function (response) {
                $elm_this.addClass("btn-is-disabled");
                if (response.code === 1) {
                    $(".loader").remove();
                    var xhtml = `<div class="alert alert-info alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>${response.msg}</strong>
                        </div>`;
                    $(xhtml).insertBefore($elm_input);
                    window.location.href = "http://lutaden.xyz/thong-bao.html";
                }
            },
        });
    });

    // Tin mới nhất dưới footer
    if ($("body, html").find("#latest_posts_footer").length > 0) {
        var numshow = 5;
        $("#latest_posts_footer").loadAjax(
            {
                controller: "bai-viet",
                action: "latest-posts-footer",
                showposts: numshow,
            },
            function (response) {
                setTimeout(() => {
                    $("#latest_posts_footer").html(response.html);
                }, 0);
            }
        );
    }
});

/*======================================*/
// cnTLoadAjax
$.fn.cnTLoadAjax = function ({ url, data }, callback) {
    var e = this[0];
    // ajax
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        data: {
            data: data,
        },
        beforeSend: function (jqXHR, settings) {
            e.classList.add("loading");
            $(e).append(
                '<div class="loading_icon"><img src="http://lutaden.xyz/news/images/preloader.gif" width="80" height="80" /></div>'
            );
        },
        success: function (response) {
            if (response.success) {
                e.classList.remove("loading");
                $(e).find(".loading_icon").remove();
            } else {
                e.parentNode.removeChild(e);
            }

            // callback
            if (typeof (callback === "function") && callback) {
                callback(response, e);
            }
        },
    });
};

$.fn.loadAjax = function ({ controller, action, showposts, data }, callback) {
    var e = this[0];
    // console.log(e);
    data = data || {};
    data.showposts = showposts;

    $(e).cnTLoadAjax(
        {
            url: root + "/" + controller + "/ajax/" + action,
            data: data,
        },
        function (response) {
            callback(response);
        }
    );
};
