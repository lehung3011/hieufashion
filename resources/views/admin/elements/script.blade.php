<!-- jQuery -->
<script src="{{ asset('admin/js/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('admin/asset/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('admin/js/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('admin/asset/nprogress/nprogress.js') }}"></script>
<!-- bootstrap-progressbar -->
<script src="{{ asset('admin/asset/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
<!-- iCheck -->
<script src="{{ asset('admin/asset/iCheck/icheck.min.js') }}"></script>
{{--  <script src="{{asset('admin/js/ckeditor/ckeditor.js')}}"></script>  --}}
<script src="{{asset('admin/js/tinymce/tinymce.min.js')}}"></script>
{{--  <script src="https://cdn.tiny.cloud/1/reeql55h81tkjxz783r72m059t3p3zur5fe3h8ci7cs9fcwl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>  --}}
<script src="{{asset('admin/js/notify.min.js')}}"></script>
<!-- Custom Theme Scripts -->

<script src="{{ asset('vendor/laravel-filemanager/js/stand-alone-button.js') }}"></script>
<script src="{{ asset('admin/js/custom.min.js') }}"></script>
<script src="{{ asset('admin/js/my-js.js') }}"></script>
@stack('extra_js')