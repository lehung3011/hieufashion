
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label_edit');

    $inputHiddenID    = Form::hidden('id', $item['id']);
    $inputHiddenTask  = Form::hidden('task', 'change-password-2');


    $elements = [
        [
            'label'   => Form::label('old_password', 'Mật khẩu cũ', $formLabelAttr),
            'element' => Form::password('old_password', $formInputAttr),
        ],[
            'label'   => Form::label('password', 'Mật khẩu mới', $formLabelAttr),
            'element' => Form::password('password', $formInputAttr),
        ],[
            'label'   => Form::label('password_confirmation', 'Nhập lại mật khẩu', $formLabelAttr),
            'element' => Form::password('password_confirmation', $formInputAttr),
        ],[
            'element' => $inputHiddenID . $inputHiddenTask . Form::submit('Cập nhật', ['class'=>'btn btn-success']),
            'type'    => "btn-submit-edit"
        ]
    ];
@endphp
@extends('admin.main')

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false])
    @include ('admin.templates.error')

    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Đổi mật khẩu'])
                <div class="x_content">
                    {{ Form::open([
                        'method'         => 'POST', 
                        'url'            => route("$controllerName/change-password-2"),
                        'accept-charset' => 'UTF-8',
                        'enctype'        => 'multipart/form-data',
                        'class'          => 'form-horizontal form-label-left',
                        'id'             => 'change-password-form',
                        'name'           => 'change-password-form' ])  }}
                        {!! FormTemplate::show($elements)  !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
