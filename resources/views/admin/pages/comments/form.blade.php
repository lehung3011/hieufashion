@extends('admin.main')
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');
    $formCkeditor  = config('zvn.template.form_ckeditor');
    $statusValue      = ['default' => 'Select status', 'active' => config('zvn.template.status.active.name'), 'inactive' => config('zvn.template.status.inactive.name')];

    $inputHiddenID    = Form::hidden('id', $item['id']);
    $inputHiddenThumb = Form::hidden('image_current', $item['image']);
  
    $elements = [
        [
            'label'   => Form::label('guest_name', 'Tên khách hàng', $formLabelAttr),
            'element' => Form::text('guest_name', $item['guest_name'],  $formInputAttr )
        ],[
            'label'   => Form::label('rate', 'Đánh giá', $formLabelAttr),
            'element' => Form::text('rate', $item['rate'],  $formInputAttr )
        ],[
            'label'   => Form::label('status', 'Trạng thái', $formLabelAttr),
            'element' => Form::select('status', $statusValue, $item['status'],  $formInputAttr)
        ],[
            'label'   => Form::label('content', 'Nhận xét', $formLabelAttr),
            'element' => Form::textArea('comment', $item['comment'],  $formCkeditor )
        ],[
            'label'   => Form::label('image', 'Ảnh', $formLabelAttr),
            'element' => FormTemplate::input_image('image', (!empty($item['id'])) ? $item['image'] : null, $formInputAttr),
            'image'   => (!empty($item['id'])) ? Template::showItemThumb($controllerName, $item['image'], $item['guest_name']) : null ,
            'type'    => "image"
        ],[
            'element' => $inputHiddenID . $inputHiddenThumb . Form::submit('Save', ['class'=>'btn btn-success']),
            'type'    => "btn-submit"
        ]
    ];
    $title_page = (isset($item) && !empty($item)) ? 'Nhận xét từ khách hàng' : 'Thêm nhận xét mới';

@endphp

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false, 'title_page' => $title_page])
    @include ('admin.templates.error')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Form'])
                <div class="x_content">
                    {{ Form::open([
                        'method'         => 'POST', 
                        'url'            => route("$controllerName/save"),
                        'accept-charset' => 'UTF-8',
                        'enctype'        => 'multipart/form-data',
                        'class'          => 'form-horizontal form-label-left',
                        'id'             => 'main-form' ])  }}
                        {!! FormTemplate::show($elements)  !!}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@endsection
