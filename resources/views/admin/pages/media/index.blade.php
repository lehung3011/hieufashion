@extends('admin.main')
@section('content')
    
    @include ('admin.templates.page_header', ['pageIndex' => false, 'title_page' => 'Thư viện hình ảnh'])
    @include ('admin.templates.zvn_notify')

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="table-responsive">
                <iframe src="/laravel-filemanager" style="width: 100%; height: 800px; overflow: hidden; border: none;"></iframe>
            </div>
        </div>
    </div>
@endsection
