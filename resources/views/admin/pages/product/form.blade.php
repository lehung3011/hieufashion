@extends('admin.main')
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;
    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');
    $formCkeditor  = config('zvn.template.form_ckeditor');
    $statusValue      = ['default' => 'Select status', 'active' => config('zvn.template.status.active.name'), 'inactive' => config('zvn.template.status.inactive.name')];
    
    $title_page = (isset($item) && !empty($item)) ? 'Chỉnh sửa sản phẩm' : 'Thêm mới sản phẩm';
    $sub_page = (isset($item) && !empty($item)) ? 'edit' : 'add';

    $inputHiddenID    = Form::hidden('id', $item['id']);
    $inputHiddenThumb = Form::hidden('picture_current', $item['picture']); 

    $elements_title = [
        [
            'label'   => null,
            'element' => FormTemplate::post_title($item['title']) . FormTemplate::post_slug($item['friendly_url'])
        ]
    ];

    $elements = [
        [
            'label'   => null,
            'element' => Form::textArea('description', $item['description'],  $formCkeditor )
        ]
    ];
    
 
    $elements_cat = [
        [
            'label'   => null,
            'element' => Form::select('cat_id', $itemsCategory, $item['cat_id'],  $formInputAttr)
        ]
    ];

    $thumb =  (!empty($item['id'])) ? Template::showItemThumb($controllerName, $item['picture'], $item['title']) : null ;
    $elements_picture = [
        [
            'label'   => null,
            'element' => FormTemplate::button_image('picture', (!empty($item['id'])) ? $item['picture'] : null, ['thumb' => $thumb]),
            'type'    => "image-button",
        ]
    ];
    
    $elements_gallery = [
        [
            'label'   => null,
            'element' => FormTemplate::button_image('picture', (!empty($item['id'])) ? $item['picture'] : null, ['thumb' => $thumb]),
            'type'    => "image-button",
        ]
    ];
    

    $elements_status = [
        [
            'label'   => null,
            'element' => Form::select('status', $statusValue, $item['status'],  $formInputAttr)
        ]
    ];
            
    $elements_submit = [
        [
            'element' => $inputHiddenID . $inputHiddenThumb . Form::button('Save', ['class'=>'btn btn-success btn-md', 'type' => 'submit', 'style' => 'width: 100%']),
            'type'    => "btn-button"
        ]
    ];
@endphp
@push('extra_js')
<script type="text/javascript">
	var action = 'product';
	var sub = '{{$sub_page}}';
</script>
@endpush
@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false, 'title_page' => $title_page])
    @include ('admin.templates.error')

    <div class="row">
        {{ Form::open([
                        'method'         => 'POST', 
                        'url'            => route("$controllerName/save"),
                        'accept-charset' => 'UTF-8',
                        'enctype'        => 'multipart/form-data',
                        'class'          => 'form-horizontal form-label-left',
                        'id'             => 'main-form' ])  }}
        <div class="col-md-10 col-sm-10 col-xs-12">
            {!! FormTemplate::show($elements_title, ['class' => 'box-post-title'])  !!}
            @include('admin.templates.form_gallery', ['gallery' => $item['image_extra']])
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Thông tin sản phẩm'])
                <div class="x_content">
                    <div class="row">
                        {!! FormTemplate::show($elements, ['class' => 'col-xs-12'])  !!}
                    </div>
                </div>
            </div>
            @include('admin.templates.form_seo', ['item' => $item])
        </div> 
        <div class="col-md-2 col-sm-2 col-xs-12">
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Chuyên mục'])
                <div class="x_content">
                    <div class="row">
                        {!! FormTemplate::show($elements_cat, ['class' => 'col-md-12 col-sm-12 col-xs-12'])  !!}
                    </div>
                </div>
            </div>
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Ảnh đại diện'])
                <div class="x_content">
                    <div class="row">
                        {!! FormTemplate::show($elements_picture, ['class' => 'col-md-12 col-sm-12 col-xs-12'])  !!}
                    </div>
                </div>
            </div>
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Trạng thái'])
                <div class="x_content">
                    <div class="row">
                        {!! FormTemplate::show($elements_status, ['class' => 'col-md-12 col-sm-12 col-xs-12'])  !!}
                    </div>
                </div>
            </div>
            {!! FormTemplate::show($elements_submit)  !!}
        </div>
        {{ Form::close() }}
    </div>
@endsection
