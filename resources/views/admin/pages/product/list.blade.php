@php
    use App\Helpers\Template as Template;
    use App\Helpers\Hightlight as Hightlight;
@endphp
<div class="x_description">
    <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
            <thead>
                <tr class="headings">
                    <th class="column-title">#</th>
                    <th class="column-title">Article Info</th>
                    <th class="column-title">Thumb</th>
                    <th class="column-title">Danh mục</th>
                    <th class="column-title">Trạng thái</th>
                    <th class="column-title">Thông tin</th> 
                    <th class="column-title">Hành động</th>
                </tr>
            </thead>
            <tbody>
                @if (count($items) > 0)
                    @foreach ($items as $key => $val)
                        @php
                            $index           = $key + 1;
                            $class           = ($index % 2 == 0) ? "even" : "odd";
                            $id              = $val['id'];
                            $title            = Hightlight::show($val['title'], $params['search'], 'title');
                            $description     = Hightlight::show($val['description'], $params['search'], 'description');
                            $thumb           = Template::showItemThumb($controllerName, $val['picture'], $val['title']);
                            $categoryList    = Template::showItemSelect2($controllerName, $id, $itemsCategory, $val['category_id'], 'category', 'change-category');
                            $status          = Template::showItemStatus($controllerName, $id, $val['status']); 
                            $modifiedHistory = Template::showItemHistory($val['modified_by'], $val['modified']);
                            $listBtnAction   = Template::showButtonAction($controllerName, $id);
                        @endphp

                        <tr class="{{ $class }} pointer">
                            <td >{{ $index }}</td>
                            <td width="30%">
                                <p><strong> {!! $title !!}</p></strong>
                            </td>
                            <td width="5%">
                                <p>{!! $thumb !!}</p>
                            </td>
                            <td >{!! $categoryList !!}</td>
                            <td>{!! $status !!}</td>
                            <td>{!! $modifiedHistory !!}</td>
                            <td class="last">{!! $listBtnAction !!}</td>
                        </tr>
                    @endforeach
                @else
                    @include('admin.templates.list_empty', ['colspan' => 8])
                @endif
            </tbody>
        </table>
    </div>
</div>
           