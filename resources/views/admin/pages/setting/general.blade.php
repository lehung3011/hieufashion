
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');

    $formtextAreaAttr = [
        'class' =>  'form-control col-md-6 col-xs-12',
        'rows'  => 4
    ];

    $inputHiddenThumb = Form::hidden('logo_current', (isset($setting['logo'])) ? $setting['logo'] : '');
    $inputHiddenKey = Form::hidden('key', $key);

    $elements_1 = [
        [
            'label'   => Form::label('indextitle', 'Site title', $formLabelAttr),
            'element' => Form::text('setting[indextitle]', isset($setting['indextitle']) ? $setting['indextitle'] : '', $formInputAttr),
        ],[
            'label'   => Form::label('metakey', 'Meta Keyword', $formLabelAttr),
            'element' => Form::textArea('setting[metakey]', isset($setting['metakey']) ? $setting['metakey'] : '', $formtextAreaAttr),
        ],[
            'label'   => Form::label('metadesc', 'Meta Description', $formLabelAttr),
            'element' => Form::textArea('setting[metadesc]', isset($setting['metadesc']) ? $setting['metadesc'] : '', $formtextAreaAttr),
        ],[
            'label'   => Form::label('meta_head', 'Meta mở rộng', $formLabelAttr),
            'element' => Form::textArea('setting[meta_head]', isset($setting['meta_head']) ? $setting['meta_head'] : '', $formtextAreaAttr),
        ],[
            'label'   => Form::label('extra_foot', 'Script mở rộng cuối trang', $formLabelAttr),
            'element' => Form::textArea('setting[extra_foot]', isset($setting['extra_foot']) ? $setting['extra_foot'] : '', $formtextAreaAttr),
        ],
    ];
    
    $elements_2 = [
        [
            'label'   => Form::label('logo', 'Logo website', $formLabelAttr), 
            'element' => FormTemplate::input_image('setting[logo]', (!empty($setting['logo'])) ? $setting['logo'] : null, $formInputAttr),
            'thumb'   => (!empty($setting['logo'])) ? Template::showItemThumb('weblink', $setting['logo'], null) : null ,
            'type'    => "thumb"
        ],[
            'label'   => Form::label('hotline', 'Hotline', $formLabelAttr),
            'element' => Form::text('setting[hotline]', isset($setting['hotline']) ? $setting['hotline'] : '', $formtextAreaAttr),
        ],[
            'label'   => Form::label('email', 'Email', $formLabelAttr),
            'element' => Form::text('setting[email]', isset($setting['email']) ? $setting['email'] : '', $formtextAreaAttr),
        ],[
            'label'   => Form::label('address', 'Địa chỉ', $formLabelAttr),
            'element' => Form::text('setting[address]', isset($setting['address']) ? $setting['address'] : '', $formtextAreaAttr),
        ]
    ];
    
    $elements_submit = [
        [
            'element' => $inputHiddenThumb . $inputHiddenKey . Form::submit('Cập nhật', ['class'=>'btn btn-success']),
            'type'    => "btn-submit-edit"
        ]
    ];

    $title_page = 'Cấu hình chung';

@endphp
@extends('admin.main')

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false, 'title_page' => $title_page, 'back' => false])
    @include ('admin.templates.error')
    @include ('admin.templates.zvn_notify')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            {{ Form::open([
                'method'         => 'POST', 
                'url'            => route("$controllerName/save",[ 'key' => $key]),
                'accept-charset' => 'UTF-8',
                'enctype'        => 'multipart/form-data',
                'class'          => 'form-horizontal form-label-left',
                'id'             => 'change-password-form',
                'name'           => 'change-password-form' ])  }}
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Cấu hình chung website'])
                <div class="x_content">
                    {!! FormTemplate::show($elements_1)  !!}
                </div>
            </div>
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Giao diện'])
                <div class="x_content">
                    {!! FormTemplate::show($elements_2)  !!}
                </div>
            </div>
            {!! FormTemplate::show($elements_submit)  !!}
            {{ Form::close() }}
        </div>
    </div>
@endsection
