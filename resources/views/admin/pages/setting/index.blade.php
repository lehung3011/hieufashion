@extends('admin.main')
@php
    use App\Helpers\Template as Template;
@endphp

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => true])
    @include ('admin.templates.zvn_notify')
@endsection
