
@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');

    $formtextAreaAttr = [
        'class' =>  'form-control col-md-6 col-xs-12',
        'rows'  => 4
    ];

    $inputHiddenThumb = Form::hidden('logo_current', (isset($setting['logo'])) ? $setting['logo'] : '');
    $inputHiddenKey = Form::hidden('key', $key);

    $elements_1 = [
        [
            'label'   => Form::label('indextitle', 'URL', $formLabelAttr),
            'element' => Form::text('setting[facebook][url]', isset($setting['facebook']['url']) ? $setting['facebook']['url'] : '', $formInputAttr),
        ],[
            'label'   => Form::label('metakey', 'Icon', $formLabelAttr),
            'element' => Form::text('setting[facebook][icon]', isset($setting['facebook']['icon']) ? $setting['facebook']['icon'] : '', $formtextAreaAttr),
        ]
    ];
    
    $elements_2 = [
        [
            'label'   => Form::label('indextitle', 'URL', $formLabelAttr),
            'element' => Form::text('setting[google][url]', isset($setting['google']['url']) ? $setting['google']['url'] : '', $formInputAttr),
        ],[
            'label'   => Form::label('metakey', 'Icon', $formLabelAttr),
            'element' => Form::text('setting[google][icon]', isset($setting['google']['icon']) ? $setting['google']['icon'] : '', $formtextAreaAttr),
        ]
    ];

    $elements_3 = [
        [
            'label'   => Form::label('indextitle', 'URL', $formLabelAttr),
            'element' => Form::text('setting[twitter][url]', isset($setting['twitter']['url']) ? $setting['twitter']['url'] : '', $formInputAttr),
        ],[
            'label'   => Form::label('metakey', 'Icon', $formLabelAttr),
            'element' => Form::text('setting[twitter][icon]', isset($setting['twitter']['icon']) ? $setting['twitter']['icon'] : '', $formtextAreaAttr),
        ]
    ];
    
    $elements_submit = [
        [
            'element' => $inputHiddenThumb . $inputHiddenKey . Form::submit('Cập nhật', ['class'=>'btn btn-success']),
            'type'    => "btn-submit-edit"
        ]
    ];

    $title_page = 'Cấu hình Mạng Xã Hội';

@endphp
@extends('admin.main')

@section('content')
    @include ('admin.templates.page_header', ['pageIndex' => false, 'title_page' => $title_page, 'back' => false])
    @include ('admin.templates.error')
    @include ('admin.templates.zvn_notify')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            {{ Form::open([
                'method'         => 'POST', 
                'url'            => route("$controllerName/save",[ 'key' => $key]),
                'accept-charset' => 'UTF-8',
                'enctype'        => 'multipart/form-data',
                'class'          => 'form-horizontal form-label-left',
                'id'             => 'change-password-form',
                'name'           => 'change-password-form' ])  }}
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Facebook'])
                <div class="x_content">
                    {!! FormTemplate::show($elements_1)  !!}
                </div>
            </div>
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Google'])
                <div class="x_content">
                    {!! FormTemplate::show($elements_2)  !!}
                </div>
            </div>
            <div class="x_panel">
                @include('admin.templates.x_title', ['title' => 'Twitter'])
                <div class="x_content">
                    {!! FormTemplate::show($elements_3)  !!}
                </div>
            </div>
            {!! FormTemplate::show($elements_submit)  !!}
            {{ Form::close() }}
        </div>
    </div>
@endsection
