@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;
    $values = (isset($item['image_extra'])) ? $item['image_extra'] : null;
    $name = 'gallery';
    $link_upload = null;
    $id = (isset($item['id'])) ? $item['id'] : 0;
@endphp

@push('extra_js')
<script src="{{asset('admin/js/upload/upload.js')}}"></script>
<script src="{{asset('admin/js/dropzone/dist/dropzone.js')}}"></script>
@endpush

@push('extra_css')
<link href="{{ asset('admin/js/upload/upload.css') }}" rel="stylesheet">
<link href="{{ asset('admin/js/dropzone/dist/dropzone.css') }}" rel="stylesheet">
@endpush

<div class="x_panel">
    @include('admin.templates.x_title', ['title' => 'Hình ảnh phụ'])
    <div class="x_content">
        <div id="gallery-box-{{$name}}" class="gallery-box">
            @if (!empty($values) && is_array($values))
            <div class="galleries">
                @foreach ($values as $i => $item)
                <div class="gallery-item gallery-item-{{$i}}">
                    <div class="inner">
                        {!!  Form::image($item['value']); !!}
                        {!!  Form::button(null, ['type' => 'button', 'class' => 'del button', 'onclick' => "UPLOAD.delete('$name',$i);"]) !!}
                        {!!  Form::hidden("{$name}[$i][title]", $item['title'] ) !!}
                        {!!  Form::hidden("{$name}[$i][value]", $item['value'] ) !!}
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            <div class="upload-btn">
                {!! link_to( $link_upload, 'Thư viện ảnh', [ 'class' => "btn-popup btn-submit btn btn-primary",  'onclick' => "return UPLOAD.popup('$name');"]
                ); !!}
                <span class="or"> Hoặc</span>
                <div class="upload_area">
                    <div class="dropzone" id="mydropzone" data-url="{{ route("ajax",['do' => 'upload_images', 'id' => $id ]) }}">
                        <input type="hidden" name="p_id" value="{{$id}}">
                        <div class="fallback">
			                <input name="file" type="file" multiple />
		                </div>
	                </div>
                </div>
            </div>
            {!!  Form::hidden("{$name}s", $item['value'], ['id' => $name, 'onchange' => "UPLOAD.change('$name');" ] ) !!}
        </div> 
    </div>
</div>