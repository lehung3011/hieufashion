@php
    use App\Helpers\Form as FormTemplate;
    use App\Helpers\Template;

    $formInputAttr = config('zvn.template.form_input');
    $formLabelAttr = config('zvn.template.form_label');
    $formTextareaAttr = config('zvn.template.form_textarea');

    $friendly_title = (isset($item['friendly_title'])) ? $item['friendly_title'] : null;
    $metakey = (isset($item['metakey'])) ? $item['metakey'] : null;
    $metadesc = (isset($item['metadesc'])) ? $item['metadesc'] : null;
    
    $elements = [
        [
            'label'   => Form::label('friendly_title', 'Tiêu đề trang', $formLabelAttr),
            'element' => Form::text('friendly_title', $friendly_title,  $formInputAttr ),
            'note'    => 'Thẻ tiêu đề trang hiệu quả phải dài từ 10-70 ký tự, bao gồm cả dấu cách.'
        ],[
            'label'   => Form::label('metadesc', 'Meta description', $formLabelAttr),
            'element' => Form::textArea('metadesc', $metadesc,  $formTextareaAttr),
            'note'    => 'Thẻ meta description hiệu quả phải dài từ 160-300 ký tự, ngắn gọn và chứa các từ khóa tốt nhất.'
        ],[
            'label'   => Form::label('metakey', 'Meta keywords', $formLabelAttr),
            'element' => Form::textArea('metakey', $metakey,  $formTextareaAttr ),
            'note'    => 'Thẻ meta keywords hiệu quả phải chứa các từ hoặc cụm từ liên quan đến nội dung của bài viết.'
        ],
    ];
@endphp
<div class="x_panel">
    @include('admin.templates.x_title', ['title' => 'SEO'])
    <div class="x_content">
         {!! FormTemplate::show($elements, ['class' => 'col-md-9 col-sm-9 col-xs-12'])  !!} 
    </div>
</div>