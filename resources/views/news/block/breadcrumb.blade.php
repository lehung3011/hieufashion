@php
    use App\Helpers\URL;
@endphp
<section class="inner-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 sec-title colored text-center">
                <h2>{!! $item['name'] !!}</h2>
                <ul class="breadcumb">
                    <li><a href="{!! route('home')!!}">Trang chủ</a></li>
                    <li><i class="fa fa-angle-right"></i></li>
                    <li><span>{!! $item['name'] !!}</span></li>
                </ul>
                <span class="decor"><span class="inner"></span></span>
            </div>
        </div>
    </div>
</section>