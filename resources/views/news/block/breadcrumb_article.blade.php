@php
    use App\Helpers\URL;
    $linkCategory  =  URL::linkCategory($item['category_id'], $item['category_name']);
@endphp

<section class="inner-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12 sec-title colored text-center">
                <h2>{!! $item['name'] !!}</h2>
                <ul class="breadcumb">
                    <li><a href="{!! route('home')!!}">Trang chủ</a></li>
                    <li><a href="{!! $linkCategory !!}">{!! $item['category_name'] !!}</a></li>
                    <li><span>{!! $item['name'] !!}</span></li>
                </ul>
                <span class="decor"><span class="inner"></span></span>
            </div>
        </div>
    </div>
</section>