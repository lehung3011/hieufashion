@php
    use App\Helpers\Template as Template;
    use App\Helpers\URL;
@endphp

<div class="single-sidebar-widget popular-post">
    <h3 class="title">Bài viết nổi bật</h3>
    <ul>
        @foreach($itemsLatest as $item) 
            @php
                $name         = $item['name'];
                $thumb        = asset($item['thumb']);
                $categoryName = $item['category_name'];
                $linkCategory = URL::linkCategory($item['category_id'], $item['category_name']);;
                $linkArticle  = URL::linkArticle($item['id'], $item['name']);
                $created      = Template::showDatetimeFrontend($item['created']);
            @endphp
            <li>
                <div class="img-box">
                    <div class="inner-box">
                        <img src="{{ $thumb }}" alt="{{ $name }}">
                    </div>
                </div>
                <div class="content-box">
                    <a href="{{ $linkArticle }}"><h4 class="zvn-title">{{ $name }}</h4></a>
                    <small>Ngày đăng: {{ $created }}</small>
                </div>
            </li>
        @endforeach
        </ul>
</div>