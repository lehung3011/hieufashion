@php
    use App\Helpers\Template as Template;
    use App\Helpers\URL;
@endphp
@isset($itemsLatest)
<div class="footer-widget latest-post itm-mgn-top-50">
    <h3 class="title">Bài viết mới nhất</h3>
    <ul>
        @foreach($itemsLatest as $item) 
        @php
            $name         = $item['name'];
            $linkArticle  = URL::linkArticle($item['id'], $item['name']);
            $created      = Template::showDatetimeFrontend($item['created']);
        @endphp
        <li>
            <span class="border"></span>
            <div class="content">
                <a href="{{ $linkArticle }}">{{  $name }}</a>
                <span>{{ $created }}</span>
            </div>
        </li>
        @endforeach
    </ul>
</div>
@endisset