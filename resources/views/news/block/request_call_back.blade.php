@php
    $setting = get_json_setting('setting-main');
    $hotline = $setting['hotline'];
    $hotline_format = str_replace(array(' ','.','-'), '', $hotline);
    $src_logo = $setting['logo'];
@endphp
<div id="myModal" class="modal fade zvn-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"> &times; </button>
                <h4 class="modal-title">Liên hệ với chúng tôi</h4>
            </div>
            <div class="modal-body">
                @isset($src_logo)
                <img src="{{ asset($src_logo) }}"  alt="Nha Khoa Lutadent"/>
                @endisset
                <p>Gọi ngay để được tư vấn & đặt lịch hẹn</p>
                <a href="tel:{{$hotline_format}}" class="thm-btn zvn-chat"><i class="fa fa-phone" aria-hidden="true"></i>
                    {{$hotline}}</a>
                <p>
                    Hoặc để lại số điện thoại của bạn để nhận cuộc gọi
                    từ Nha Khoa Lutadent
                </p>
                <input type="text" name="phone_customer" placeholder="Số điện thoại của bạn"/>
                <a href="javascript::void()" class="thm-btn zvn-call zvn-submit-phone">
                    Yêu cầu gọi lại</a>
            </div>
        </div>
    </div>
</div>