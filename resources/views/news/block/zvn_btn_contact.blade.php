@php
    $hotline = get_json_setting('setting-main','hotline');
@endphp
<div class="zvn-btn-contact hidden-md">
    @isset($hotline)
    <a href="javascript::void()" data-toggle="modal" data-target="#myModal"  class="thm-btn zvn-call">
        <i class="fa fa-phone" aria-hidden="true"></i>
        {{$hotline}}
    </a> 
    @endisset
    <a href="{{ route('booking')}}" class="thm-btn zvn-chat">Đặt lịch hẹn</a>
</div>