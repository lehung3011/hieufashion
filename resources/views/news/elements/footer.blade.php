@php
use App\Models\AgenciesModel;
use App\Models\ArticleModel;
use App\Helpers\Template as Template;
$info_basic = Config::get('lutaden');

$articleModel = new ArticleModel(); 
$itemsService = $articleModel->listItems([  'category_id'  => 3], ['task' => 'news-list-items-in-category']);  

$social = $info_basic['social'];
if(!empty($social))
{
    $list_social = Template::showListSocial($social);
}
@endphp
<footer class="footer sec-pdd-35 zvn-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-6">
                <div class="footer-widget about-widget">
                    <a href="https://nhakhoalutadent.com">
                        <img src="https://nhakhoalutadent.com/dentist/img/resources/logo.png" style="width: 50%; min-width: unset;" alt="Awesome Image" />
                    </a>
                    <ul class="contact">
                        @isset($info_basic['hotline'])
                            <li>
                                <i class="fa fa-phone"></i>
                                <span>{{ $info_basic['hotline'] }}</span>
                            </li>
                        @endisset
                        @isset($info_basic['email'])
                            <li>
                                <i class="fa fa-envelope-o"></i>
                                <span><a href="mailto:{{ $info_basic['email'] }}">{{ $info_basic['email'] }}</a></span>
                            </li>
                        @endisset
                    </ul>
                    {!! $list_social !!}
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                @include('news.block.our_service')
            </div>
            <div class="col-md-3 latest-post col-sm-6 hidden-xs">
                <div id="latest_posts_footer"></div>
            </div>
        </div>
    </div>
</footer>
<a href="#" id="back-to-top" title="Back to top">
    <i class="fa fa-angle-up"></i>
    <p>TOP</p>
</a>
<section class="footer-bottom">
    <div class="container text-center">
        <p> Xây dựng và phát triển bởi
            <a href="http://lanluu.tech/" target="_blank">lanluu.tech</a>
        </p>
    </div>
</section>