@php
use App\Models\ArticleModel;
use App\Helpers\URL as URL;
$info_basic = Config::get('lutaden');
$articleModel = new ArticleModel();
$hotline = $info_basic['hotline'];
@endphp
<header class="header pd-b-xs-0">
    <div class="container">
        @include('news.block.logo')
        @include('news.elements.header_right')
    </div>
</header>

<nav class="mainmenu-area stricky"> 
    @include('news.block.zvn_btn_contact')
    <div class="container">
        <div class="navigation pull-left">
            <div class="nav-header">
                @include('news.elements.menu_main')
            </div>
            <div class="nav-footer">
                <button><i class="fa fa-bars"></i></button>
            </div>
        </div>
    </div>
</nav>