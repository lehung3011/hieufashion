@php
use App\Helpers\URL as URL;
use App\Models\MenuModel;
//use App\Models\CategoryModel;
use App\Models\ArticleModel;

$menuModel = new MenuModel;
$listMenu  = $menuModel->createMenusMain();
foreach ($listMenu as $k => &$val){
    if($val['sub'] === null)
    {
        $link = json_decode($val['link']);
        $type = $link->type;
        $value = $link->value;

        switch($type) :
            case 'category':
                //$categoryModel = new CategoryModel;
                $itemCategory = $categoryModel->getItem([ 'category_id'  => $value], ['task' => 'news-get-item']); 
                $category_id = $itemCategory['id'];
                $category_name = $itemCategory['name'];
                $linkCategory = URL::linkCategory($category_id,$category_name);
                $val['link'] = $linkCategory;

                $sub = null;
                $articleModel = new ArticleModel;
                $items = $articleModel->getItem([ 'id'  => $category_id ], ['task' => 'get-by-category-id']); 
                foreach ($items as $item)
                {
                    $id = $item->id;
                    $name = $item->name;
                    $linkDetail = URL::linkArticle($id,$name);
                    $child = [
                        'name'      =>  $name,
                        'link'      =>  $linkDetail,
                        'target'    =>  $val['target']
                    ];
                    if(isset($val['sub']))
                    {
                        array_push($val['sub'],$child);
                    }
                    else{
                        $val['sub'][] = $child;
                    }
                }
            break;
            case 'news':
                $articleModel = new ArticleModel;
                $itemArticle = $articleModel->getItem([ 'article_id'  => $value], ['task' => 'news-get-item']); 
                $id = $itemArticle['id'];
                $name = $itemArticle['name'];
                $linkDetail = URL::linkArticle($id,$name);
                $val['link'] = $linkDetail;
            break;
            default:
                $val['link'] = $value;
            break;    
        endswitch;    
    }
}
@endphp
<ul class="zvn-menu">
    @foreach ($listMenu as $item)
        @if ($item['sub'] !== null)
            <li class="dropdown">
                <a href="{{ $item['link'] }}" target="{{ $item['target'] }}">{{ $item['name'] }}</a>
                @isset($item['sub'])
                    <ul class="submenu">
                        @foreach ($item['sub'] as $sub_item)
                        <li>
                            <a href="{{ $sub_item['link'] }}" target="{{ $sub_item['target'] }}">{{ $sub_item['name'] }}</a>
                        </li>
                        @endforeach
                    </ul>
                @endisset
            </li>
        @else
            <li><a href="{{ $item['link'] }}" target="{{ $item['target'] }}">{{ $item['name'] }}</a></li>
        @endif
    @endforeach
</ul>