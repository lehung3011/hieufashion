@if(count($related_articles) > 0)
    <div class="sec-pdd-90 meet-doctors zvn-related">
        <div class="container">
            <div class="sec-title text-left"><h2>Bài viết liên quan</h2><span class="decor"><span class="inner"></span></span></div>
        </div>
        <div class="clearfix">
            <div class="team-carousel">
                @include('news.pages.article.child-index.category_grid')   
            </div>
        </div>
    </div>
@endif