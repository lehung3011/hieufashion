@php
    use App\Helpers\URL;
@endphp
@isset($category_list)
 <div class="single-sidebar-widget category">
    <h3 class="title">Chuyên mục</h3>
    <ul>

        @foreach ($category_list as $item)
            @php
                $link = URL::linkCategory($item['id'], $item['name']);
                $name = $item['name'];
            @endphp
            <li><a class="zvn-title" href="{{ $link }}">{{ $name }}</a></li>    
        @endforeach
    </ul>
</div>
@endisset