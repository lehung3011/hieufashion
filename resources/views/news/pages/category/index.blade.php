@extends('news.main')
@push('extra_js')
<script src="{{asset('news/js/article.js')}}"></script>
@endpush
@section('content')
<div class="section-category">
    @include('news.block.breadcrumb', ['item' => $itemCategory])
    <div class="blog-home sec-pdd-90 blog-page blog-details zvn-blog-details ">
        <div class="container">
            <div class="row">
                <div class="col-md-8 pull-left">
                    @include('news.pages.category.child-index.category', ['item' => $itemCategory]) 
                </div>
                <div class="col-md-4 col-sm-12 pull-right">
                    <div id="box-list-category"></div>
                    <div class="side-bar-widget itm-mgn-top-50">
                        <div id="article_latest_posts"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection