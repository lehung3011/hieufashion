@extends('news.main')
@section('content')
@include('news.block.breadcrumb', ['item' => ['name' => 'Câu hỏi thường gặp']])
<div class="sec-pdd-90 faq-home faq-page">
    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('news.pages.faqs.child-index.list_faqs') 
                </div>
            </div>
        </div>
</div>
@endsection