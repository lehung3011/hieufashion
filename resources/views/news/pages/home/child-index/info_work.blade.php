@php
    $info_basic = Config::get('lutaden');
    $opening = $info_basic['opening'];
    $note_datlichhen = $info_basic['note_datlichhen'];
@endphp
<section class="call-to-action home-one">
    <div class="container-fluid">
        <div class="clearfix">
            <div class="call-to-action-corner col-md-4">
                <div class="single-call-to-action open-hours">
                    <div class="icon-box">
                        <div class="inner-box">
                            <i class="flaticon-square"></i>
                        </div>
                    </div>
                    <div class="content-box">
                        <h3>Giờ mở cửa</h3>
                        <ul>
                            @foreach ($opening as $item)
                                <li>
                                    <span>{{$item['day']}}</span>
                                    <span>{{$item['time']}}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="call-to-action-center col-md-4" style="background-image: url(img/call-to-action/center-box-bg.jpg);">
                <div class="single-call-to-action">
                    <div class="icon-box">
                        <div class="inner-box">
                            <i class="flaticon-day-15-on-calendar"></i>
                        </div>
                    </div>
                    <div class="content-box">
                        <h3>Đặt lịch hẹn</h3>
                        <p style="color: #ffffff !important;">{{$note_datlichhen}}</p>
                        <a href="https://nhakhoalutadent.com/dat-lich-hen.html"  class="thm-btn inverse">Đặt lịch hẹn</a>
                    </div>
                </div>
            </div>
            <div class="call-to-action-corner col-md-4"  style="background-image: url(img/call-to-action/right-box-bg.jpg);">
                <div class="single-call-to-action">
                    <div class="icon-box">
                        <div class="inner-box">
                            <i class="flaticon-coin-of-dollar"></i>
                        </div>
                    </div>
                    <div class="content-box">
                        <h3>Thanh toán</h3>
                        <p style="color: #ffffff !important;">Thanh toán trực tiếp hoặc chuyển khoản ngân hàng </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>