@extends('news.main')
@section('content')
@include('news.block.breadcrumb', ['item' => ['name' => 'Thông báo !']])
<section class="error-section">
    <div class="container">
        <div class="zvn-card">
            <div class="zvn-card-body">
                <h5 class="card-title">Cảm ơn bạn đã để lại thông tin!</h5>
                <p class="card-text">Nha khoa Lutadent sẽ liên hệ đến bạn trong thời gian sớm nhất</p>
                <p class="card-text"><i class="fa fa-phone" aria-hidden="true"></i> Hotline: 02822670999</p>
                <a href="http://lutaden.xyz/" class="btn btn-primary">Trở về trang chủ</a>
            </div>
        </div>
    </div>
</section>
@endsection